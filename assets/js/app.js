import '../css/app.scss';
import $ from 'jquery'
window.jQuery = $;
global.$ = global.jQuery = $;

import * as firebase from 'firebase/app';
import 'firebase/auth';

import firebaseConfig from "./firebaseconfig";


firebase.initializeApp(firebaseConfig);

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');


$(function () {
    $('#form').submit(function (e) {
        e.preventDefault();
    });
    $('#anterior').change(function () {
        if($(this).val() != 0){
            window.location = $(this).val();
        }
    })
    $('#cerrar_sesion').click(function (e) {
        console.log("cerrar sesion");
        e.preventDefault();
        firebase.auth().signOut();
        window.location = '/logout';
    });
    $(".usuario-tool").click(function(){
        if($(this).hasClass("menu-open")){
            $(this).removeClass("menu-open");
        } else {
            $(this).addClass("menu-open");
        }
    });
    $(".menu-mobile").click(function(){
        if($(this).hasClass("menu-open-dos")){
            $(this).removeClass("menu-open-dos");
            $(".menue").removeClass("menu-open-dos");
        } else {
            $(this).addClass("menu-open-dos");
            $(".menue").addClass("menu-open-dos");
        }
    });
});

function setLatLng(latLng) {
    $('#lat').val(latLng.lat());
    $('#lng').val(latLng.lng());
}

/**
 * Handles the sign up button press.
 */
function handleSignUp() {
    showLoader();
    $('#errores').hide();
    var nombre = document.getElementById('nombre').value;
    var apellido = document.getElementById('apellido').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var password_second = document.getElementById('password_second').value;
    if (nombre.length < 2) {
        setError('Ingresa tu nombre');
        return;
    }
    if (nombre.length < 3) {
        setError('Ingresa tu apellido');
        return;
    }
    if (email.length < 4) {
        setError('Ingresa tu email');
        return;
    }
    if (password.length < 4) {
        setError('Ingresa tu contraseña');
        return;
    }
    if (password != password_second) {
        setError('Las contraseñas deben coincidir');
        return;
    }
    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
            setError('La contraseña debe tener al menos 6 caracteres');
        } else {
            setError(errorMessage);
        }
        console.log(error);
    });
}
/**
 * Handles the sign in button press.
 */
function toggleSignIn() {
    showLoader();
    $('#errores').hide();
    if (firebase.auth().currentUser) {
    } else {
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        if (email.length < 4) {
            setError('Ingresa tu email');
            return;
        }
        if (password.length < 4) {
            setError('Ingresa tu contraseña');
            return;
        }
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/wrong-password') {
                setError('Contraseña Inválida');
            } else {
                setError(errorMessage);
            }
            console.log(error);
            document.getElementById('quickstart-sign-in').disabled = false;
        });
    }
    document.getElementById('quickstart-sign-in').disabled = true;
}

function sendPasswordReset() {
    showLoader();
    $('#errores').hide();
    var email = document.getElementById('email').value;
    // [START sendpasswordemail]
    firebase.auth().sendPasswordResetEmail(email).then(function() {
        $('#success').show();
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/invalid-email') {
            setError('Email inválido');
        } else if (errorCode == 'auth/user-not-found') {
            setError('Usuario no encontrado');
        }
        console.log(error);
        // [END_EXCLUDE]
    });
    // [END sendpasswordemail];
}

function setError(mensaje) {
    $('.loader-wrapper').hide();
    $('#errores').show().html(mensaje);
    window.scrollTo(0, 0);
}

function showLoader() {
    $('.loader-wrapper').show();
}


function initApp() {
    // Listening for auth state changes.
    // [START authstatelistener]
    firebase.auth().onAuthStateChanged(function(user) {
        // [START_EXCLUDE silent]
        //document.getElementById('quickstart-verify-email').disabled = true;
        // [END_EXCLUDE]
        if (user) {
            // User is signed in.
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            var user_up = {};
            if($('#nombre').length > 0){
                var nombre = document.getElementById('nombre').value;
                user_up.nombre = nombre;
            }
            if($('#apellido').length > 0){
                var apellido = document.getElementById('apellido').value;
                user_up.apellido = apellido;
            }
            user_up.email = email;
            user_up.uid = uid;
            user_up.providerData = providerData;
            user_up.telefono = user.telefono;
            console.log(user);

            $.ajax({
                type: "POST",
                url: "/api/auth/register",
                // The key needs to match your method's input parameter (case-sensitive).
                data: JSON.stringify(user_up),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){
                    console.log(data);
                    window.location = data.url;
                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        } else {
        }
    });

    if($('#quickstart-sign-in').length > 0)
        document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
    if($('#quickstart-sign-up').length > 0)
        document.getElementById('quickstart-sign-up').addEventListener('click', handleSignUp, false);
    //document.getElementById('quickstart-verify-email').addEventListener('click', sendEmailVerification, false);
    if($('#quickstart-password-reset').length > 0){
        document.getElementById('quickstart-password-reset').addEventListener('click', sendPasswordReset, false);
    }
}

window.onload = function() {
    console.log('ini fire');
    if(window.location.href.includes('crear-cuenta') || window.location.href.includes('ingresar') || window.location.href.includes('olvide')) {
        initApp();
    }
};
