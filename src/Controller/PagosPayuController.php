<?php

namespace App\Controller;

use App\Entity\Compra;
use App\Entity\CompraItem;
use App\Entity\Direccion;
use App\Entity\EstadoPedido;
use App\Entity\Inventario;
use App\Entity\MetodoPago;
use App\Entity\PagoPayu;
use App\Entity\PayuLog;
use App\Entity\Producto;
use App\Entity\Tienda;
use App\Service\QI;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PagosPayuController extends AbstractController
{
    private  $qi;
    private $userManager;
    private $retryTtl;
    private $tokenGenerator;
    private $eventDispatcher;
    private $authorization;

    public function __construct(QI $qi, UserManagerInterface $userManager,TokenGeneratorInterface $tokenGenerator, EventDispatcherInterface $eventDispatcher,AuthorizationCheckerInterface $authorization)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
        $this->retryTtl = 7200;
        $this->tokenGenerator = $tokenGenerator;
        $this->eventDispatcher = $eventDispatcher;
        $this->authorization = $authorization;
    }

    /**
     * @Route("/pagos/payu", name="pagos_payu")
     */
    public function index(Request $request)
    {
        $metodo = $this->getDoctrine()->getRepository(MetodoPago::class)->findOneBy(array('id' => '2'));
        $canal = "web";
        $comentarios = $request->request->get('comentarios',"");
        $res = $this->qi->crearPedido($metodo,$canal,$comentarios,$this->getUser());
        $compra = $res["compra"];
        $descripcion_text = $res["descripcion"];

        $test=$this->qi->getSetting("modo_pruebas_payu");
        $sufix = $test == 1 ? '_prueba':'';
        $tax = $compra->getImpuestos();
        $taxReturnBase = round(($compra->getTotal()-$compra->getCostoDomicilio()-$tax),2);
        $referenceCode = $this->qi->getSetting('projectId').'_'.$test.'_'.$compra->getId();
        $merchantId= $this->qi->getSetting('payuMerchantId'.$sufix);
        $apikey = $this->qi->getSetting('payuApiKey'.$sufix);
        $accountId = $this->qi->getSetting('payuAccountId'.$sufix);
        $currency = 'COP';
        $firma = md5($apikey.'~'.$merchantId.'~'.$referenceCode.'~'.round($compra->getTotal(),2).'~'.$currency);

        //$this->qi->clearCar("","");
        //$this->qi->clearCar("","1");

        $ar= [
            'compra'=>$compra,
            'tax'=>$tax,
            'test'=>$test,
            'taxReturnBase'=>$taxReturnBase,
            'firma'=>$firma,
            'referenceCode'=>$referenceCode,
            'descripcion_text'=>substr($descripcion_text,0,250),
            'merchantId'=>$merchantId,
            'apiKey'=>$apikey,
            'accountId'=>$accountId,
            'currency' => $currency,
            'nombreCiudad'=>$compra->getDireccion()->getCiudad()->getNombre(),
            'shipping' => round($compra->getCostoDomicilio(),2)
        ];

        return $this->render('pagos_payu/index.html.twig', $ar);
    }

    /**
     * @Route("/payu-confirmacion",name="pagar_payu_confirmacion", methods={"POST"})
     */
    public function confirmacionPayu(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $log = new PayuLog();
        $full_url = serialize($request->request->all());
        $log->setData($full_url);
        $em->persist($log);
        $em->flush();


        $ApiKey = $this->qi->getSetting('payu_apiKey');
        $merchant_id = $this->qi->getSetting('payu_merchantId');
        $referenceCode = $request->request->get('reference_sale');

        $TX_VALUE = $request->request->get('value');
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = 'COP';
        $transactionState = $request->request->get('state_pol');

        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->request->get('sign');
        if (strtoupper($firma) == strtoupper($firmacreada)  ) {

            $transactionState = $request->request->get('state_pol');
            if ($transactionState == 4) {
                $estadoTx = "APROBADA";
            } else if ($transactionState == 6) {
                $estadoTx = "RECHAZADA";
            } else if ($transactionState == 104) {
                $estadoTx = "PENDIENTE";
            } else if ($transactionState == 7) {
                $estadoTx = "ERROR";
            } else {
                $estadoTx = "ERROR";
            }

            $referenceCode = $request->request->get('reference_sale');
            $arr_ref = explode('_',$referenceCode);
            $id_sus = end($arr_ref);

            $compra = $this->getDoctrine()->getRepository(Compra::class)->find($id_sus);
            $pago = new PagoPayu();
            $pago->setIdPayu($request->request->get('transaction_id'));
            $pago->setReferenceSale($request->request->get('reference_sale'));
            $pago->setMonto($request->request->get('value'));
            $pago->setCus($request->request->get('cus'));
            $pago->setReferencePol($request->request->get('reference_pol'));
            $fecha = new \DateTime($request->request->get('transaction_date'));
            $pago->setFecha($fecha);
            $pago->setState($request->request->get('response_message_pol'));
            $pago->setCompra($compra);


            $em->persist($pago);
            $em->flush();

            if ($transactionState == 4) {
                $estado = $this->getDoctrine()->getRepository(EstadoPedido::class)->findOneBy(array('ref'=>'pagada'));
                $compra->setEstado($estado);
                $em->persist($compra);
                $em->flush();

                if($this->qi->getSetting('enviar_correo') == "1"){
                    $asunto = $this->qi->getTexto('asunto_compra_cliente');
                    $this->qi->sendMail($asunto, $this->getUser()->getEmail(), $compra->getBody());
                    $asunto = $this->qi->getTexto('asunto_compra_tienda');
                    if($compra->getTienda()) {
                        $this->qi->sendMail($asunto, $compra->getTienda()->getEmail(), $compra->getBody());
                    }else{
                        $this->qi->sendMail($asunto, $this->qi->getSetting('mail_recepcion_pedidos'), $compra->getBody());
                    }
                }

            }
            return new Response('Hello Payu', Response::HTTP_OK);
        }else{
            return new Response('Hello Payu', Response::HTTP_OK);
        }
        return new Response('Hello Payu', Response::HTTP_OK);
    }

    /**
     * @Route("/payu-respuesta",name="pagar_payu_respuesta")
     */
    public function respuestaPayu(Request $request)
    {
        $test=$this->qi->getSetting("modo_pruebas_payu");
        $sufix = $test == 1 ? '_prueba':'';
        $ApiKey = $this->qi->getSetting('payuApiKey'.$sufix);
        $merchant_id = $this->qi->getSetting('payuMerchantId'.$sufix);
        $referenceCode = $request->query->get('referenceCode');
        $TX_VALUE = $request->query->get('TX_VALUE');
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = 'COP';
        $transactionState = $request->query->get('transactionState');

        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->query->get('signature');

        if (strtoupper($firma) == strtoupper($firmacreada)  ) {

            $transactionState = $request->query->get('transactionState');
            if ($transactionState == 4) {
                $estadoTx = "APROBADA";

            } else if ($transactionState == 6) {
                $estadoTx = "RECHAZADA";
            } else if ($transactionState == 104) {
                $estadoTx = "PENDIENTE";
            } else if ($transactionState == 7) {
                $estadoTx = "ERROR";
            } else {
                $estadoTx = "ERROR";
            }

            if( $test == "1") {
                $estado = $this->getDoctrine()->getRepository(EstadoPedido::class)->findOneBy(array("ref" => $estadoTx));
                $referenceCode = $request->query->get('referenceCode');
                $arr_ref = explode('_', $referenceCode);
                $id_compra = end($arr_ref);
                /* @var $compra Compra */
                $compra = $this->getDoctrine()->getRepository(Compra::class)->find($id_compra);
                $compra->setEstado($estado);
                $cus = $request->query->get('cus');
                $compra->setEstadoProcesamiento($cus);
                $processingDate = $request->query->get('processingDate');
                $compra->setFechaRealProcesamiento(new \DateTime($processingDate));
                $em = $this->getDoctrine()->getManager();
                $em->persist($compra);
                $em->flush();
            }
            return $this->render('basic/pedido_respuesta.html.twig', [
                'ok' => $estadoTx == "APROBADA"
            ]);


        }else{
            return $this->render('basic/pedido_respuesta.html.twig', [
                'ok' => false
            ]);
        }
    }


}
