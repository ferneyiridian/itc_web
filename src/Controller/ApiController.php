<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index()
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    /**
     * @Route("/api/productos/{tienda}/{categoria}/{page}/{size}", name="api")
     */
    public function productos($tienda, $categoria, $page, $size)
    {
        $first = ($page - 1) * $size;
        $productos = $this->getDoctrine()->getRepository('App:Producto')->createQueryBuilder('p')
            ->select('p.plu','p.imagen', 'p.pum', 'p.nombre','i.precio','p.costo','p.resumen')
            ->leftJoin('p.inventarios','i')
            ->leftJoin('i.tienda','t')
            ->leftJoin('p.categorias','c')
            ->where('i.cantidad > 0')
            ->andWhere('c.id = '.$categoria)
            ->andWhere("t.id = '".$tienda."'")
            ->setFirstResult($first)
            ->setMaxResults($size)
            ->groupBy('p.plu')
            ->getQuery()
            ->getArrayResult();
        $json = json_encode($productos);
        return new Response($json);
    }
}
