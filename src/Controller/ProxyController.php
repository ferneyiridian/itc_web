<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;

class ProxyController extends AbstractController
{
    /**
     * @Route("/proxy", name="proxy")
     */
    public function index(Request $request)
    {
        try {
            // $_GET parameters
            $url = $request->query->get('url', '');
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $url);

            $sfresp = new Response($response->getContent(), $response->getStatusCode(), array(
                'Content-Type' => 'application/json'
            ));

            return $sfresp;

        } catch (\Throwable $e) {

            $sfresp = new Response(json_encode(array('message' => $e->getMessage())), 400, array(
                'Content-Type' => 'application/json'
            ));
            return $sfresp;

        }
    }

}
