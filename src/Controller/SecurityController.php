<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 25/04/2019
 * Time: 7:25 PM
 */

namespace App\Controller;

use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController;
use AlterPHP\EasyAdminExtensionBundle\Security\AdminAuthorizationChecker;
use App\Entity\Ciudad;
use App\Entity\Color;
use App\Entity\Especialidad;
use App\Entity\Masivo;
use App\Entity\Medico;
use App\Entity\Producto;
use App\Entity\Usuario;
use App\Service\QI;
use Doctrine\ORM\Query\ResultSetMapping;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
#use function Google\Cloud\Samples\Datastore\properties;
#use Kreait\Firebase;
use mysqli;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/loginn", name="login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('FOSUserBundle:Security:login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }
}
