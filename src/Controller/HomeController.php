<?php


namespace App\Controller;
use App\Entity\Categoria;
use App\Entity\Ciudad;
use App\Entity\Compra;
use App\Entity\CompraItem;
use App\Entity\Direccion;
use App\Entity\EstadoPedido;
use App\Entity\Inventario;
use App\Entity\MetodoPago;
use App\Entity\Producto;
use App\Entity\Tienda;
use App\Entity\Usuario;
use App\Form\DireccionType;
use App\Service\QI;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use phpDocumentor\Reflection\Types\This;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class HomeController extends AbstractController
{
    private  $qi;
    private $userManager;
    private $retryTtl;
    private $tokenGenerator;
    private $eventDispatcher;
    private $authorization;

    public function __construct(QI $qi, UserManagerInterface $userManager,TokenGeneratorInterface $tokenGenerator, EventDispatcherInterface $eventDispatcher,AuthorizationCheckerInterface $authorization)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
        $this->retryTtl = 7200;
        $this->tokenGenerator = $tokenGenerator;
        $this->eventDispatcher = $eventDispatcher;
        $this->authorization = $authorization;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function home(Request $request)
    {
        $user = $this->getUser();
        if ($this->qi->getSetting('registro_obligatorio') == '1'){
            if(!$user){
                return $this->redirectToRoute('ingresar');
            }
        }
        if ($this->qi->getSetting('tienda_redirige') == '1' && $this->qi->getSetting('muestra_tiendas') != "0") {
            $tienda = $this->qi->getTienda();
            if ($tienda) {
                return $this->redirectToRoute('categorias', array('id' => $tienda->getId()));
            }
        }

        if ($this->qi->getSetting('registro_obligatorio') != '1'){
            $first = 1;
            $size = 50;
            $productos = $this->getDoctrine()->getRepository('App:Inventario')->createQueryBuilder('i')
                ->select('i.precio','p.nombre','p.imagen','p.plu')
                ->leftJoin('i.producto','p')
                ->leftJoin('p.categorias','c')
                ->where('i.cantidad > 0')
                ->andWhere('p.visible = 1')
                ->setFirstResult($first)
                ->setMaxResults($size)
                ->getQuery()
                ->getArrayResult();
            shuffle($productos);
            $productos = array_slice($productos,0,4);
            $categorias = $this->qi->getCategoriasConProductos();

            return $this->render('home/home_new.html.twig', [
                'productos' => $productos,
                'categorias' => $categorias
            ]);
        }else{
            $direcciones = $this->getDoctrine()->getRepository(Direccion::class)->findBy(array('usuario'=>$user));
            return $this->render('basic/addDir.html.twig', [
                'direcciones' => $direcciones,
            ]);
        }
        //dd($tiendas);
    }


    /**
     * @Route("/misdirecciones", name="misdirecciones")
     */
    public function misdirecciones(Request $request)
    {
        $user = $this->getUser();
        if(!$user){
            return $this->redirectToRoute('ingresar');
        }

        $tiendas = $this->getDoctrine()->getRepository(Tienda::class)->findBy(array('visible'=>true),array('orden'=>'asc'));
        $direcciones = $this->getDoctrine()->getRepository(Direccion::class)->findBy(array('usuario'=>$user));
        //dd($tiendas);
        return $this->render('basic/addDir.html.twig', [
            'tiendas' => $tiendas,
            'direcciones' => $direcciones
        ]);
    }

    /**
     * @Route("/contacto", name="contacto")
     */
    public function contacto(Request $request)
    {
        /*$contacto= new Contacto();
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contacto);
            $entityManager->flush();
            $asunto = $this->qi->getSetting('asunto_mail_contacto');
            $receiver = $this->qi->getSetting('receiver_mail_contacto');
            $this->qi->sendMail($asunto,$receiver,$contacto->getHtml());
            return $this->render('contacto/contacto.html.twig', [
                'gracias'=>true
            ]);
        }
        return $this->render('basic/contacto.html.twig', [
            'gracias'=>false,
            'form'=>$form->createView()
        ]);
        */
    }


    /**
     * @Route("/pedir", name="pedir")
     */
    public function pedir(Request $request)
    {

        try {
            $user = $this->getUser();
            $direccion = $this->getDoctrine()->getRepository(Direccion::class)->find($this->qi->getDireccion()->getId());
            $metodo_sel = $request->request->get('metodo');
            $metodo = $this->getDoctrine()->getRepository(MetodoPago::class)->findOneBy(array('id' => $metodo_sel));
            $tienda = $this->getDoctrine()->getRepository(Tienda::class)->find($this->qi->getTienda()->getId());
            $costo_domicilio = $tienda->getCostoDomicilio();
            $comentarios = $request->request->get('comentarios');
            $peticion= $this->qi->getCarrito();
            $paga_con = 0;
            $canal = "web";
            $carrito=[];
            if($metodo_sel == "2"){
                return $this->redirectToRoute('pagos_payu',array("comentarios"=>$comentarios));
            }
            foreach ($this->qi->getCarrito() as $item){
                $elem = array("product_id"=>$item["producto"]->getPlu(),"count"=>$item["cantidad"]);
                array_push($carrito,$elem);
            }

            $res = $this->qi->crearPedido($metodo,$canal,$comentarios,$this->getUser());


            $compra =$res["compra"];

            $resultado = array("ok" => true, 'obj' => array("compra"=>array("id"=>$compra->getId())));


            if($this->qi->getSetting('enviar_correo') == "1"){
                $asunto = $this->qi->getTexto('asunto_compra_cliente');
                $this->qi->sendMail($asunto, $this->getUser()->getEmail(), $compra->getBody());
                $asunto = $this->qi->getTexto('asunto_compra_tienda');
                if($compra->getTienda()) {
                    $this->qi->sendMail($asunto, $compra->getTienda()->getEmail(), $compra->getBody());
                }else{
                    $this->qi->sendMail($asunto, $this->qi->getSetting('mail_recepcion_pedidos'), $compra->getBody());
                }
            }

        }catch (\Exception $e){
            $resultado = array("ok" => false, 'obj' => $e->getCode()." - ".$e->getMessage());
        }

        if($resultado["ok"]) {
            return $this->redirectToRoute('confirmacion', $resultado);
        }else{
            return $this->redirectToRoute('carrito', $resultado);
        }
    }

    /**
     * @Route("/set-direccion/{id}", name="set-direccion")
     */
    public function setDireccion(Request $request,$id)
    {
        $user = $this->getUser();
        $dir = $this->getDoctrine()->getRepository(Direccion::class)->find($id);
        $this->qi->setDireccion($dir);
        if($this->qi->carritoLength() > 0)
            return $this->redirectToRoute('carrito');
        return $this->redirectToRoute('tiendas');
    }

    /**
     * @Route("/add-direccion", name="add-direccion")
     */
    public function addDireccion(Request $request)
    {
        $user = $this->getUser();
        $dir = new Direccion();
        $dirarr = $request->request->get('direccion');
        $tipo = $request->request->get('direccion')['tipocalle'];
        $base = $request->request->get('direccion')['base'];
        $numero = $request->request->get('direccion')['numero'];
        $detalle = $request->request->get('direccion')['detalle'];
        $adicional = $request->request->get('direccion')['adicional'];
        $dirarr['direccion']=$tipo." ".$base." # ".$numero." - ".$detalle." ".$adicional;

        $ciudad= $this->getDoctrine()->getRepository(Ciudad::class)->find(1);
        $request->request->set('direccion',$dirarr);
        $dir->setUsuario($user);
        $dir->setDireccion($dirarr['direccion']);
        $dir->setLng($dirarr['lng']);
        $dir->setLat($dirarr['lat']);
        $dir->setNombre($dirarr['nombre']);
        $dir->setUid($user->getUid());
        $dir->setCiudad($ciudad);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($dir);
        $entityManager->flush();


        $this->qi->setDireccion($dir);
        return $this->redirectToRoute('tiendas');
    }



    /**
     * @Route("/tiendas", name="tiendas")
     */
    public function tiendas(Request $request)
    {
        /*if( $this->qi->getSetting('muestra_tiendas') == "0"){
            return $this->redirectToRoute('homepage');
        }
        */

        $user = $this->getUser();
        $tiendas = $this->getDoctrine()->getRepository('App:Tienda')->findBy(array('visible'=>true));
        if(count($tiendas)== 1){
            $this->qi->setTienda($tiendas[0]);
            return $this->redirectToRoute('categorias', array('id' => $tiendas[0]->getId()));
        }
        $enZona = [];
        $dir= $this->qi->getDireccion();
        if($this->qi->getSetting('tienda_redirige') == 1){
            if(!$dir){
                return $this->redirectToRoute('misdirecciones');
            }
        }
        foreach ($tiendas as $t){
            if($dir){
                if($this->arePointsNear($t,$dir,$t->getRadioCobertura()  ) || true ){
                    array_push($enZona,$t);
                }
            }else{
                array_push($enZona,$t);
            }
        }
        return $this->render('basic/tiendas.html.twig', [
            'tiendas' => $enZona
        ]);
    }


    /**
     * @Route("/productos", name="productos")
     * @Route("/categoria/{categoria_id}", name="categorias_notienda")
     * @Route("/tienda/{id}", name="categorias")
     * @Route("/tienda/{id}/categoria/{categoria_id}", name="categorias_tienda")
     */
    public function categoriasTienda(Request $request,$id = null, $categoria_id = null)
    {
        $paginator = [];
        $size = 50;
        $page = $request->query->get('page',1);
        $query = $request->query->get('query',"");
        $first = ($page - 1)*$size;
        $user = $this->getUser();
        $dir= $this->qi->getDireccion();
        if($id != null){
            $tienda = $this->getDoctrine()->getRepository('App:Tienda')->find($id);
            $this->qi->setTienda($tienda);
        }else{
            $tienda = null;
        }


        $productos = $this->getDoctrine()->getRepository('App:Inventario')->createQueryBuilder('i')
            ->select('i.precio','p.nombre','p.imagen','p.plu')
            ->leftJoin('i.producto','p')
            ->leftJoin('p.categorias','c')
            ->where('i.cantidad > 0')
            ->setFirstResult($first)
            ->setMaxResults($size);

        $cant = $this->getDoctrine()->getRepository('App:Inventario')->createQueryBuilder('i')
            ->select('p.plu')
            ->leftJoin('i.producto','p')
            ->leftJoin('p.categorias','c')
            ->where('i.cantidad > 0');
        if($id){
            $productos->andWhere('i.tienda = :tienda')
                ->setParameter('tienda',$tienda);
            $cant->andWhere('i.tienda = :tienda')
                ->setParameter('tienda',$tienda);
        }
        if($categoria_id){
            $productos->andWhere('c.id = '.$categoria_id);
            $cant->andWhere('c.id = '.$categoria_id);
        }

        if($query != ""){
            $parts = explode(" ",$query);
            $cad = [];
            foreach ($parts as $parte){
                $cad[] = " p.nombre like '%$parte%'";
            }
            $q_search = implode($cad," or ");
            if(count($cad) > 0){
                $productos->andWhere($q_search);
                $cant->andWhere($q_search);
            }
        }
        $productos = $productos->getQuery()->getArrayResult();
        $cant=$cant->getQuery()->getArrayResult();
        if($id)
            $categorias = $this->qi->getCategoriasVeci();
        else
            $categorias = $this->qi->getCategoriasConProductos();

        $cant= count($cant);
        $paginator['hasPreviousPage'] = $page > 1;
        $paginator['hasNextPage'] = $page < $cant/$size;
        $paginator['cant'] = $cant;
        $paginator['results'] = $cant;
        $paginator['nextPage'] = $page + 1;
        $paginator['previousPage'] = $page - 1;
        $paginator['pages'] = ceil($cant/$size) ;
        $paginator['page'] = $page;

        return $this->render('basic/categoriasTienda.html.twig', [
            'query'=>$query,
            'categorias'=> $categorias,
            'categoria'=> null,
            'categoria_id'=> null,
            'productos' => $productos,
            'cant' => $cant,
            'tienda' => $tienda,
            'paginator' => $paginator
        ]);
    }

    function existeCatProds($productos,$c){
        $encontrado = false;
        for ($i = 0; $i<count($productos);$i++){
            for($k = 0; $k < count($productos[$i]["producto"]["categorias"]);$k++){
                $encontrado = $c->getId() == $productos[$i]["producto"]["categorias"][$k]['id'];
                if($encontrado){

                    $k=count($productos[$i]["producto"]["categorias"])+1;
                    $i=count($productos)+1;
                    break;
                }
            }
            if($encontrado){
                break;
            }


        }
        return $encontrado;
    }

    function arePointsNear($checkPoint, $centerPoint, $km) {
        $ky = 40000 / 360;
        $kx = cos(pi() * $centerPoint->getLat() / 180.0) * $ky;
        $dx = abs($centerPoint->getLng() - $checkPoint->getLng()) * $kx;
        $dy = abs($centerPoint->getLat() - $checkPoint->getLat()) * $ky;
        return sqrt($dx * $dx + $dy * $dy) <= $km;
    }

    /**
     * @Route("/categoria", name="categoria")
     * @Route("/categoria/{id}/{tiendaId}", name="categoria_id")
     */
    public function categoria(Request $request, $id = null,$tiendaId = null)
    {
        if(!$id) {
            $tienda = null;
            $categoria = $this->getDoctrine()->getRepository('App:Categoria')->findOneBy(array('visible' => true));
        }
        else {
            $tienda = $this->getDoctrine()->getRepository('App:Tienda')->find($tiendaId);
            $categoria = $this->getDoctrine()->getRepository('App:Categoria')->find($id);
        }
        $categorias = $this->qi->getCategoriasVeci();
        $productos = $this->getDoctrine()->getRepository('App:Inventario')->createQueryBuilder('i')
            ->select('i.precio','p.nombre','p.imagen','p.plu')
            ->leftJoin('i.producto','p')
            //->addSelect('p')
            ->leftJoin('p.categorias','c')
            //->addSelect('c')
            ->where('i.tienda = :tienda')
            ->andWhere('i.cantidad > 0')
            ->andWhere('c.id = :cat')
            ->setParameter('tienda',$tienda)
            ->setParameter('cat',$categoria)
            ->getQuery()
            ->getArrayResult();
        return $this->render('basic/categoriasTienda.html.twig', [
            'categorias'=> $categorias,
            'categoria'=> $categoria,
            'categoria_id'=> $categoria->getId(),
            'productos' => $productos,
            'cant' => count($productos),
            'tienda' => $tienda
        ]);
    }

    /**
     * @Route("/producto/{id}/{tiendaId}", name="producto")
     */
    public function producto(Request $request, $id,$tiendaId)
    {
        $tienda = $this->getDoctrine()->getRepository('App:Tienda')->find($tiendaId);
        $productos = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->from('App:Inventario','i')
            ->select('i')
            ->leftJoin('i.producto','p')
            ->addSelect('p')
            ->leftJoin('p.categorias','c')
            ->addSelect('c')
            ->where('i.tienda = :tienda')
            ->andWhere('i.cantidad > 0')
            ->andWhere('p.plu = :id')
            ->setParameter('tienda',$tienda)
            ->setParameter('id',$id)
            ->getQuery()
            ->getArrayResult();
        return $this->render('basic/producto.html.twig', [
            'producto' => $productos[0]
        ]);
    }

    /**
     * @Route("/add-to-carrito", name="add-to-carrito")
     */
    public function addtocarrito(Request $request)
    {
        $body= json_decode($request->getContent());
        $mensaje=array('ok' => false, 'mensaje' => "Ha ocurrido un error");
        if($this->qi->addCantToCar($body->producto,$body->cantidad,$body->wish)) {
            $mensaje = array('ok' => true, 'mensaje' => "Agregado al carrito ",'carrito'=>count($this->qi->getCarrito()));
            if ($body->wish) {
                $mensaje = array('ok' => true, 'mensaje' => "Agregado al wish list");
            }
        }

        $response = new Response();
        $response->setContent(json_encode($mensaje));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/eliminar-carro/{id}", name="eliminar-carro")
     */
    public function eliminarDeCarro(Request $request,$id)
    {

        $this->qi->deleteItemFromCar($id,"");
        return $this->redirectToRoute('carrito');
    }

    /**
     * @Route("/cambiar-cantidad/{id}/{cantidad}", name="cambiar-cantidad")
     */
    public function cambiarCantidad(Request $request,$id,$cantidad)
    {

        if($cantidad > 0) {
            $this->qi->addItemToCar($id, $cantidad, "");
        }
        else{
            $this->qi->deleteItemFromCar($id,  "");
        }
        return $this->redirectToRoute('carrito');
    }

    /**
     * @Route("/carrito", name="carrito")
     */
    public function carrito(Request $request)
    {
        $ok = true;

        if($request->get('ok') != null ){
            $ok = $request->get('ok');
        }
        if(!$this->qi->getDireccion()){
            return $this->redirectToRoute('misdirecciones');
        }
        if(!$this->qi->getTienda()){
            return $this->redirectToRoute('tiendas');
        }
        $productos = $this->getDoctrine()->getRepository('App:Producto')->
        findBy(array('visible'=>true),array('orden'=>'desc'),7, rand(1,1500));
        return $this->render('basic/carrito.html.twig', [
            'productos' => $productos,
            'ok'=>$ok
        ]);
    }

    /**
     * @Route("/pedido_respuesta", name="pedido_respuesta")
     */
    public function pedido_respuesta(Request $request)
    {
        $ok = true;

        if($request->get('ok') != null ){
            $ok = $request->get('ok');
        }
        if(!$this->qi->getDireccion()){
            return $this->redirectToRoute('misdirecciones');
        }
        if(!$this->qi->getTienda()){
            return $this->redirectToRoute('tiendas');
        }
        return $this->render('basic/pedido_respuesta.html.twig', [
            'ok'=>$ok
        ]);
    }

    /**
     * @Route("/mapa_tiendas", name="mapaTiendas")
     */
    public function mapaTiendas(Request $request)
    {

        $tiendas = $this->getDoctrine()->getRepository(Tienda::class)->findAll();

        return $this->render('basic/mapaTiendas.html.twig', [
            'tiendas' => $tiendas
        ]);
    }

    /**
     * @Route("/direccion", name="direccion")
     */
    public function direccion(Request $request)
    {
        $productos = $this->getDoctrine()->getRepository('App:Producto')->
        findBy(array('visible'=>true),array('orden'=>'desc'),7, rand(1,1500));
        return $this->render('basic/direccion.html.twig', [
            'productos' => $productos
        ]);
    }

    /**
     * @Route("/confirmacion", name="confirmacion")
     */
    public function confirmation(Request $request)
    {

        $respuesta = $request->get('obj');
        $compra = $this->getDoctrine()->getRepository(Compra::class)->find($respuesta["compra"]["id"]);

        $productos = $this->qi->getCarrito();
        $this->qi->clearCar("");


        return $this->render('basic/pedido_respuesta.html.twig', [
            'productos' => $productos,
            'compra' => $compra,
            'ok' => $request->get("ok")

        ]);
    }

    /**
     * @Route("/ingresar", name="ingresar")
     */
    public function ingresar(Request $request)
    {
        $errores = [];
        $last_username = '';
        if($request->isMethod('POST')){
            $datos = $request->request->all();
            $email = strtolower($datos['_username'] );
            $last_username = $email;
            $pass = strtolower($datos['_password'] );
            try {
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../miveci-firebase-adminsdk-pteyb-91be53dc7d.json');
                $firebase = (new Factory())
                    ->withServiceAccount($serviceAccount)
                    ->create();
                $auth = $firebase->getAuth();
                $fbuser = $auth->getUserByEmail($email);
                $user = $this->userManager->findUserBy(array('email'=>$email));

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));
                $event = new InteractiveLoginEvent($request, $token);
                //$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                $this->eventDispatcher->dispatch("security.interactive_login", $event);
                if ($this->get('security.context')->isGranted('ROLE_POS_ADMIN')) {
                    return $this->redirectToRoute('easyadmin');
                }else{
                    return $this->redirectToRoute('homepage');
                }

            } catch (Firebase\Exception\AuthException $e) {
                array_push($errores, $e->getMessage());
                //echo($e->getMessage());
                //echo(dump($e));
                //exit();
            } catch (Firebase\Exception\FirebaseException $e) {
                array_push($errores, $e->getMessage());
            }

        }
        return $this->render('user/login.html.twig', [
            'errores' => $errores,
            'last_username' => $last_username
        ]);
    }

    /**
     * @Route("/olvide", name="olvide")
     */
    public function olvide(Request $request)
    {
        return $this->render('user/olvide.html.twig', [
        ]);
    }

    /**
     * @Route("/crear-cuenta", name="crear_cuenta")
     */
    public function crearCuenta(Request $request)
    {
        $errores = [];
        if($request->isMethod('POST')){
            $datos = $request->request->all();
            $properties = [
                'displayName' => $datos['nombre'] . " " . $datos['apellido'] ,
                'email' => strtolower($datos['email'] ),
                'emailverified' => true
            ];
            try {
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../miveci-firebase-adminsdk-pteyb-91be53dc7d.json');
                $firebase = (new Factory())
                    ->withServiceAccount($serviceAccount)
                    ->create();
                $auth = $firebase->getAuth();
                $fbuser = $auth->createUserWithEmailAndPassword($datos['email'], $datos['apellido']);
                $auth->setCustomUserAttributes($fbuser->uid, $properties);

                $user = new Usuario();
                $body = $fbuser;
                $user->setUid($body->uid);
                $user->setEmail($body->email);
                $user->setUsername($body->email);
                $user->setUsernameCanonical($body->email);
                $user->setProviderData(json_encode($body->providerData));
                $user->setEnabled(true);
                $user->setPassword($datos['password_first']);
                $user->setCreatedAt(new \DateTime());
                $user->setNombre($datos['nombre']);
                $user->setApellido($datos['apellido']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $this->qi->saveFire($user);

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));
                $event = new InteractiveLoginEvent($request, $token);
                //$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                $this->eventDispatcher->dispatch("security.interactive_login", $event);

                return $this->redirectToRoute('homepage');

            } catch (Firebase\Exception\AuthException $e) {
                array_push($errores, $e->getMessage());
                //echo($e->getMessage());
                //echo(dump($e));
                //exit();
            } catch (Firebase\Exception\FirebaseException $e) {
                array_push($errores, $e->getMessage());
            }

        }
        return $this->render('user/registro.html.twig', [
            'errores' => $errores
        ]);
    }

    /**
     * @Route("/load", name="load")
     */
    public function load(Request $request)
    {

        /*
        $prods = $this->getDoctrine()->getRepository(Inventario::class)->findAll();

        foreach ($prods as $inv){
            $this->qi->saveFire($inv);
        }
    */
        dd(12);
        //return $this->redirectToRoute('easyadmin');
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * @Route("/privacidad", name="privacidad")
     */
    public function privacidad(Request $request)
    {
        return $this->render('basic/privacidad.html.twig', [
            'llave'=>'privacidad',
            'titulo'=> 'Políticas de privacidad'
        ]);
    }

    /**
     * @Route("/terminos", name="terminos")
     */
    public function terminos(Request $request)
    {
        return $this->render('basic/privacidad.html.twig', [
            'llave'=>'terminos',
            'titulo'=> 'Términos y condiciones'
        ]);
    }

    /**
     * @Method({"POST"})
     * @Route("/api/auth/register", name="crear_actualizar_usuario")
     */
    public function crearActualizarUsuario(Request $request)
    {
        $um = $this->userManager;
        $body = json_decode($request->getContent());
        //dd($body);
        $user = $um->findUserBy(array('uid'=>$body->uid));
        if(!$user){
            $user = $um->findUserBy(array('email'=>$body->email));
        }
        if(!$user){
            $user = $um->createUser();
            $user->setUid($body->uid);
            $user->setEmail($body->email);
            $user->setUsername($body->email);
            $user->setUsernameCanonical($body->email);
            $user->setProviderData(json_encode($body->providerData));
            $user->setEnabled(true);
            $user->setPassword('redes123');
            $user->setCreatedAt(new \DateTime());

        }
        if(property_exists($body,'uid')){
            $user->setUid($body->uid);
        }
        if(property_exists($body,'nombre')){
            $user->setNombre($body->nombre);
        }
        if(property_exists($body,'apellido')){
            $user->setApellido($body->apellido);
        }
        /*
        if($body->celular){
            $user->setCelular($body->celular);
        }
        */

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $um->updateUser($user);
        $this->qi->saveFire($user);


        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        //$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        $this->eventDispatcher->dispatch("security.interactive_login", $event);

        if ($this->authorization->isGranted('ROLE_POS_ADMIN')) {
            $resultado= array("ok"=>true,'obj'=>$user,'url'=> $this->generateUrl('easyadmin'));
        }else{
            if($this->qi->carritoLength() > 0){
                if($this->qi->getDireccion())
                    $ruta = $this->generateUrl('carrito');
                else
                    $ruta = $this->generateUrl('misdirecciones');
            }
            else
                $ruta = $this->generateUrl('homepage');
            $resultado= array("ok"=>true,'obj'=>$user,'url'=> $ruta);
        }



        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Method({"POST"})
     * @Route("/api/direccion", name="crear_direccion")
     */
    public function crearDireccion(Request $request)
    {
        $body = json_decode($request->getContent());
        //dd($body);
        $qi = $this->qi;
        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array('uid'=>$body->uid));
        $ciudad = $this->getDoctrine()->getRepository(Ciudad::class)->find($body->ciudad->id);
        $news = new Direccion();
        $news->setUsuario($user);
        $news->setNombre($body->nombre);
        $news->setCiudad($ciudad);
        $news->setDireccion($body->direccion." ".$body->adicional);
        $news->setLat($body->lat);
        $news->setLng($body->lng);
        $news->setUid($body->uid);
        $news->setPredeterminada($body->predeterminada);
        $em = $this->getDoctrine()->getManager();
        $em->persist($news);
        $em->flush();
        $qi->saveFire($news);
        $resultado= array("ok"=>true,'obj'=>$news);

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Method({"POST"})
     * @Route("/api/historial/{uid}", name="historial")
     */
    public function historial(Request $request,$uid)
    {
        //$body = json_decode($request->getContent());
       // dd($uid);
        $qi = $this->qi;
        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array('uid'=>$uid));
        $compras = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->from('App:Compra','c')
            ->addSelect('c')
            ->leftJoin('c.tienda','t')
            ->addSelect('t')
            ->leftJoin('c.direccion','d')
            ->addSelect('d')
            ->where('c.usuario = :usuario')
            ->setParameter('usuario',$user)
            ->getQuery()
            ->getArrayResult();
        //dd($compras);
        $resultado= array("ok"=>true,'historial'=>$compras);

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Method({"POST"})
     * @Route("/api/pedido/new", name="pedido_new")
     */
    public function newPedido(Request $request)
    {
        $body = json_decode($request->getContent());
        //dd($body);

        try {
            $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array('uid' => $body->usuario_id));
            $direccion = $this->getDoctrine()->getRepository(Direccion::class)->findOneBy(array('id' => $body->direccion_id));
            $metodo = $this->getDoctrine()->getRepository(MetodoPago::class)->findOneBy(array('id' => $body->metodo_id));
            $tienda = $this->getDoctrine()->getRepository(Tienda::class)->findOneBy(array('id' => $body->tienda_id));
            $costo_domicilio = $body->costo_domicilio;
            $comentarios = $body->comentarios;
            $peticion= $body;
            $paga_con = $body->paga_con;
            $carrito= $body->carrito;
            $canal = "app";
            $resultado= $this->crearPedido($user,$direccion,$metodo,$tienda,$costo_domicilio,$comentarios,$peticion,$paga_con,$carrito,$canal);
            if($resultado['ok']){
                $asunto = $this->qi->getTexto('asunto_compra_cliente');
                if($this->qi->getSetting('enviar_correo') == "1"){
                    $this->qi->sendMail($asunto, $user->getEmail(), $this->qi->getTextoBig('mail_compra_cliente'));
                }
                $asunto = $this->qi->getTexto('asunto_compra_tienda');
                //$this->qi->sendMail($asunto,$this->getUser()->getEmail(),$this->qi->getTextoBig('mail_compra_cliente') );
            }
        }catch (\Exception $e){
            $resultado = array("ok" => false, 'obj' => $e->getCode()." - ".$e->getMessage());
        }

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    function crearPedido($user,$direccion,$metodo,$tienda,$costo_domicilio,$comentarios,$peticion,$paga_con,$carrito,$canal){


        $qi = $this->qi;
        $em = $this->getDoctrine()->getManager();
        $estado = $this->getDoctrine()->getRepository(EstadoPedido::class)->findOneBy(array('ref' => 'creado'));
        $subtotal = 0;
        $total = 0;
        $news = new Compra();
        $news->setCreatedAt(new \DateTime());
        $news->setDireccion($direccion);
        $news->setUsuario($user);
        $news->setFechaRealProcesamiento(new \DateTime());
        $news->setEstado($estado);
        $news->setEstadoProcesamiento("nuevo");
        $news->setCostoDomicilio($costo_domicilio);
        $news->setComentarios($comentarios);
        $news->setBody(json_encode($peticion));
        $news->setCanal($canal);
        $news->setMetodo($metodo);
        $news->setPagaCon($paga_con);
        $news->setSubtotal($subtotal);
        $news->setTienda($tienda);

        $total = $subtotal + $costo_domicilio;
        $news->setTotal($total);
        $em->persist($news);
        $em->flush();
        foreach ($carrito as $item) {
            if ($canal == 'app') {
                $product_id = $item->product_id;
                $count = $item->count;
            } else {
                $product_id = $item["product_id"];
                $count = $item["count"];
            }
            $prod = $this->getDoctrine()->getRepository(Producto::class)->findOneBy(array('plu' => $product_id));
            if($this->qi->getSetting('pedido_multitienda') != '1')
                $inv = $this->getDoctrine()->getRepository(Inventario::class)->findOneBy(array('producto' => $prod, 'tienda' => $tienda));
            else
                $inv = $this->getDoctrine()->getRepository(Inventario::class)->findOneBy(array('producto' => $prod));
            if ($inv) {
                $subtotal = $subtotal + $inv->getPrecio() * $count;
                $newci = new CompraItem();
                $newci->setProducto($prod);
                $newci->setPrecio($inv->getPrecio() * $count);
                $newci->setCantidad($count);
                $newci->setCompra($news);
                $newci->setCosto($inv->getPrecio());
                $em->persist($newci);
                $em->flush();
                $qi->saveFire($newci);
            } else {

            }

        }
        $news->setSubtotal($subtotal);
        $total = $subtotal + $costo_domicilio;
        $news->setTotal($total);
        $em->persist($news);
        $em->flush();
        $qi->saveFire($news);
        $obj = array('tienda' => $tienda,
            'compra' => array('id' => $news->getId(),
                'total' => $news->getTotal(),
                'estado' => $news->getEstadoProcesamiento(),
                'subtotal' => $news->getSubtotal(),
                'costoDomicilio' => $news->getCostoDomicilio(),
            )
        );
        $resultado = array("ok" => true, 'obj' => $obj);





        return $resultado;
    }

    /**
     * @Route("/import", name="import")
     */
    public function import(Request $request)
    {
        /*$categorias = $this->getDoctrine()->getRepository('App:Producto')->findAll();
        foreach ($categorias as $categoria){
            $this->qi->saveFire($categoria);
        }*/
        /*
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        foreach ($data as $producto){
            $producto_in = new Producto();
            $producto_in->setPlu($producto['sku']);
            $producto_in->setNombre($producto['nombreEs']);
            $producto_in->setImagen($producto['imagen']);
            $producto_in->setOrden(1);
            $producto_in->setPrecio(0);
            $producto_in->setVisible(true);
            if(is_numeric($producto['id'])){
                $categoria = $this->getDoctrine()->getRepository('App:Categoria')->find($producto['id']);
                $producto_in->addCategoria($categoria);
            }
            $em->persist($producto_in);
        }
        $em->flush();
        die();

        foreach ($data as $categoria){
            $categoria_in = new Categoria();
            $categoria_in->setNombre($categoria['nombre']);
            $categoria_in->setImagen($categoria['imagen']);
            $categoria_in->setVisible(true);
            $categoria_in->setOrden($categoria['orden']);
            $em->persist($categoria_in);
        }
        $em->flush();
        die();
        return $this->redirectToRoute('easyadmin');
        */
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    }



    /**
     * @Route("/resetting/iridian", name="reset-iridian")
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');
        $user = $this->userManager->findUserByUsernameOrEmail($username);
        $event = new GetResponseNullableUserEvent($user, $request);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        if (null !== $user && !$user->isPasswordRequestNonExpired($this->retryTtl)) {
            $event = new GetResponseUserEvent($user, $request);
            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }
            $event = new GetResponseUserEvent($user, $request);
            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
            $this->sendResettingEmailMessage($user);
            $user->setPasswordRequestedAt(new \DateTime());
            $this->userManager->updateUser($user);
            $event = new GetResponseUserEvent($user, $request);
            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
        }

        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('username' => $username)));
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {

        $url = $this->generateUrl('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $mensaje= str_replace('%link%','<a href="'.$url.'">link</a>',$this->qi->getTextoBig('mail_contrasena'));
        //$this->qi->sendMailPHP($this->qi->getTexto('asunto_mail_resetting'),$user->getEmail(),$mensaje) ;
    }



    /**
     * @Route("/fullreload", name="fullreload")
     */
    /*
    public function fullreload(Request $request)
    {
        $productos = $this->getDoctrine()->getRepository('App:Producto')
            ->createQueryBuilder('p')
            ->select('p','c as cats')
            ->leftJoin('p.categorias','c')
            ->where('c.id is not null')
            //->setMaxResults(2)
            ->getQuery()
            ->getArrayResult();
        $prods = array();
        foreach ($productos as $producto){
            $cat = $producto['categorias'][0]['id'];
            $producto['categorias'] = array($cat);
            $prods[$producto['plu']] = $producto;
        }
        $this->qi->saveMultiFire('Producto',$prods);
        dd($prods);
        return $this->render('basic/template.html.twig', [
        ]);
    }
    */




    /**
     * @Route("/load", name="load")
     */
    /*
    public function load(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $precios = array(10000, 5000, 7900, 12000, 17000, 20500,9000, 500, 300, 1000, 14500);
        $productos = $this->getDoctrine()->getRepository('App:Producto')->findAll();
        $tiendas = $this->getDoctrine()->getRepository('App:Tienda')->findAll();
        foreach ($tiendas as $tienda){
            $cont = 0;
            foreach ($productos as $producto){
                if($cont >= count($precios))
                    $cont = 0;
                $inventario = new Inventario();
                $inventario->setCantidad(1);
                $inventario->setProducto($producto);
                $inventario->setPrecio($precios[$cont]);
                $inventario->setTienda($tienda);
                $cont++;
                $em->persist($inventario);
                $em->flush();
                $this->qi->saveFire($inventario);
            }
        }
        return $this->render('basic/template.html.twig', [
        ]);
    }
    */


}
