<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 25/04/2019
 * Time: 7:25 PM
 */

namespace App\Controller;

use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController;
use AlterPHP\EasyAdminExtensionBundle\Security\AdminAuthorizationChecker;
use App\Entity\Inventario;
use App\Entity\POSAdmin;
use App\Entity\Producto;
use App\Entity\Setting;
use App\Entity\SuperGrupo;
use App\Entity\Usuario;
use App\Service\QI;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
use Kreait\Firebase;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminController extends EasyAdminController
{
    private $validator;
    private $firebase;
    private $userManager;
    private $qi;

    /**
     * @Route("/", name="easyadmin")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ForbiddenActionException
     */
    public function indexAction(Request $request)
    {
        $this->initialize($request);

        if (null === $request->query->get('entity')) {
            return $this->redirectToBackendHomepage();
        }else{
            $entity = $request->query->get('entity');
            /* @var $user Usuario */
            $user = $this->getUser();
            if($user->hasRole('ROLE_POS_ADMIN')){
                $allowedEntities = ['Inventario','Compra'];
                if(!in_array($entity,$allowedEntities) ){
                    return $this->redirectToRoute('easyadmin',array('action'=>'list','entity'=>'Inventario'));
                }
            }
        }

        $action = $request->query->get('action', 'list');
        if (!$this->isActionAllowed($action)) {
            throw new ForbiddenActionException(['action' => $action, 'entity_name' => $this->entity['name']]);
        }

        return $this->executeDynamicMethod($action.'<EntityName>Action');
    }


    private function reporteNativoGenFull($cols,$cadena){
        $rsm = new ResultSetMapping();
        foreach ($cols as $col){
            $rsm->addScalarResult($col, $col, 'string');
        }
        $reporte = $this->getDoctrine()->getManager()->createNativeQuery($cadena, $rsm)->getArrayResult();
        return $reporte;
    }



    protected function listInventarioAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        /* @var $user Usuario */
        $user = $this->getUser();
        $usuario_tiendas = $this->getDoctrine()->getRepository('App:POSAdmin')->findOneBy(array('id'=>$user->getId()));
        $tienda_user = false;

        if($usuario_tiendas)
            $tienda_user = $usuario_tiendas->getTienda();
        $paginator = [];
        $fields = ['id','nombre','imagen','disponible','precio'];
        if($tienda_user){
            $tienda_user_id = $tienda_user->getId();
            if($this->request->isMethod('POST')){
                $data = $this->request->request->get('inventario');
                //dd($data);
                $repo_producto = $this->getDoctrine()->getRepository('App:Producto');
                $repo_inventario = $this->getDoctrine()->getRepository('App:Inventario');
                $tienda = $tienda_user;

                foreach ($data as $plu => $datos){
                    $disponible = $datos['disponible'];
                    $producto = $repo_producto->find($plu);
                    $inventario = $repo_inventario->findOneBy(array('producto'=>$producto,'tienda'=>$tienda));
                    if($disponible){
                        $precio = (float)$datos['precio'];
                        if(!$inventario)
                            $inventario = new Inventario();
                        $inventario->setTienda($tienda);
                        $inventario->setPrecio($precio);
                        $inventario->setCantidad($disponible);
                        $inventario->setProducto($producto);
                        $this->em->persist($inventario);
                        $this->qi->saveFire($inventario);
                    }else{
                        if($inventario){
                            $this->qi->removeFire('Inventario',$inventario->getId());
                            $this->em->remove($inventario);
                        }
                    }
                    $this->em->flush();


                }
                $setting = $this->em->getRepository(Setting::class)->findOneBy(array('name'=>'actualizacion_prods_'.$tienda->getId()));
                if(!$setting){

                    $setting = new Setting();
                    $setting->setName('actualizacion_prods_'.$tienda->getId());
                }
                $ahora = new \DateTime();
                $setting->setValue($ahora->getTimestamp());
                $this->em->persist($setting);
                $this->em->flush();
                $this->qi->saveFire($setting);

                $categorias = $this->getDoctrine()->getRepository('App:Categoria')->createQueryBuilder('c')
                    ->select('c.id')
                    ->leftJoin('c.productos','p')
                    ->leftJoin('p.inventarios','i')
                    ->leftJoin('i.tienda','t')
                    ->where('i.cantidad > 0')
                    ->andWhere("t.id = '".$tienda->getId()."'")
                    ->groupBy('c.id')
                    ->getQuery()
                    ->getArrayResult();
                $cats = [];
                foreach ($categorias as $categoria){
                    array_push($cats,$categoria['id']);
                }
                $this->qi->saveFire($tienda, array('categorias'=>$cats));

                $act_cat = $this->getDoctrine()->getRepository('App:Setting')->findOneBy(array('name'=>'actualizacion_categorias'));
                if(!$act_cat){
                    $act_cat = new Setting();
                }
                $fecha = new \DateTime();
                $act_cat->setValue($fecha->getTimestamp());
                $act_cat->setUpdatedAt(new \DateTime());
                $act_cat->setName('actualizacion_categorias');
                $this->em->persist($act_cat);
                $this->em->flush();
                $this->qi->saveFire($act_cat);

                $this->addFlash(
                    'success',
                    'Los datos se guradaron correctamente'
                );
            }


            //$paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);


            $page = $this->request->query->get('page', 1);
            $cat_id = $this->request->query->get('cat', null);
            $search = $this->request->query->get('query', null);
            $w_cad = ' where ';
            $busqueda = '';
            $and = '';
            if($search){
                $busqueda = $w_cad." p.nombre like '%$search%'";
                $w_cad = '';
                $and = ' and ';
            }else{
                if($cat_id){
                    $busqueda = $w_cad." pc.categoria_id = ".$cat_id;
                    $and = ' and ';
                }
            }
            if($this->qi->getSetting('solo_inventarios_de_mi_tienda') == 1){
                $busqueda = $w_cad.$and." i.tienda_id = ".$tienda_user->getId();
            }
            $size = 30;
            $offset = ($page - 1)*$size;
            $q = "SELECT count(*) cant FROM producto p left join producto_categoria pc on pc.producto_id = p.id left join inventario i on i.producto_id = p.id and i.tienda_id = '".$tienda_user_id."' ".$busqueda;
            $cant = intval($this->reporteNativoGenFull(['cant'], $q)[0]['cant']);
            $pages = intval($cant / $size) + 1;

            $q = "SELECT p.id, p.nombre, p.imagen, i.cantidad disponible, i.precio  FROM producto p left join producto_categoria pc on pc.producto_id = p.id left join inventario i on i.producto_id = p.id and i.tienda_id = '".$tienda_user_id."' ".$busqueda." limit $offset,$size";
            $paginator['currentPageResults'] = $this->reporteNativoGenFull($fields, $q);
            $paginator['hasPreviousPage'] = $page > 1;
            $paginator['hasNextPage'] = $page < $cant/$size;
            $paginator['cant'] = $cant;
            $paginator['results'] = $cant;
            $paginator['nextPage'] = $page + 1;
            $paginator['previousPage'] = $page - 1;
        }else{
            $paginator['currentPageResults'] = [];
            $paginator['hasPreviousPage'] = false;
            $paginator['hasNextPage'] = false;
            $paginator['cant'] = 0;
            $paginator['results'] = 0;
            $paginator['nextPage'] = 1;
            $paginator['previousPage'] = 1;
        }


        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];



        return $this->executeDynamicMethod('render<EntityName>Template', ['list', "easyadmin/inventarios.html.twig", $parameters]);
    }


    protected function initialize(Request $request)
    {
        parent::initialize($request);
    }

    protected function listProductoAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);
        $categoria_id = $this->request->query->get('cat', null);
        $fields = $this->entity['list']['fields'];
        //$this->entity['list']['dql_filter'] = "entity.categorias.id = 31 ";
        $paginator = $this->findAllProds($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter'],$categoria_id);
        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    /**
     * Performs a database query to get all the records related to the given
     * entity. It supports pagination and field sorting.
     *
     * @param string      $entityClass
     * @param int         $page
     * @param int         $maxPerPage
     * @param string|null $sortField
     * @param string|null $sortDirection
     * @param string|null $dqlFilter
     *
     * @return Pagerfanta The paginated query results
     */
    protected function findAllProds($entityClass, $page = 1, $maxPerPage = 15, $sortField = null, $sortDirection = null, $dqlFilter = null, $categoria_id = null)
    {
        if (null === $sortDirection || !\in_array(\strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }

        $queryBuilder = $this->executeDynamicMethod('create<EntityName>ListQueryBuilder', [$entityClass, $sortDirection, $sortField, $dqlFilter]);

        $user = $this->getUser();
        /*$queryBuilder = $this->em->getRepository('App:Producto')->createQueryBuilder('p')
        ->leftJoin('p.inventarios','i')
        ->leftJoin('i.tienda','t')
        ->andWhere('t.id = '.$user->getTienda()->getId());*/
        if($categoria_id){
            $queryBuilder = $queryBuilder->leftJoin('entity.categorias','c')->andWhere('c.id = '.$categoria_id);
        }
        if(method_exists($user,'getTienda') && $user->getTienda()){
            $queryBuilder = $queryBuilder->leftJoin('entity.inventarios','i')
                ->leftJoin('i.tienda','t')
                ->andWhere('t.id = '.$user->getTienda()->getId());
        }
        $this->dispatch(EasyAdminEvents::POST_LIST_QUERY_BUILDER, [
            'query_builder' => $queryBuilder,
            'sort_field' => $sortField,
            'sort_direction' => $sortDirection,
        ]);
        return $this->get('easyadmin.paginator')->createOrmPaginator($queryBuilder, $page, $maxPerPage);
    }


    protected function listCompraAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $tienda = null;
        if(method_exists($this->getUser(),"getTienda"))
            $tienda = $this->getUser()->getTienda();
        if($tienda){
            //test
            $this->entity['list']['dql_filter'] = "entity.tienda = '".$tienda->getId()."'";
        }else{
            //$this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    protected function listIntentoAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $id_compra = $this->request->query->get('id_compra', 1);
        if($id_compra != 1){
            $this->entity['list']['dql_filter'] = "entity.compra = ".$id_compra;
        }else{
            $this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    public function supergruposAction()
    {
        // controllers extending the base AdminController get access to the
        // following variables:
        //   $this->request, stores the current request
        //   $this->em, stores the Entity Manager for this Doctrine entity

        // change the properties of the given entity and save the changes
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(Producto::class)->find($id);
        $supergrupos = $this->em->getRepository(SuperGrupo::class)->findAll();

        // redirect to the 'list' view of the given entity ...
        return $this->render('easyadmin/supergrupos.html.twig',array('producto'=>$entity,'supergrupos'=>$supergrupos));
    }

    /**
     * AdminController constructor.
     */
    public function __construct(ValidatorInterface $validator, UserManagerInterface $userManager, QI $qi)
    {
        $this->validator = $validator;
        $this->userManager = $userManager;
        $this->qi = $qi;
    }


    public function createNewUsuarioEntity()
    {
        $this->userManager->createUser();
    }

    public function persistUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::persistEntity($user);
    }

    public function updateUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::updateEntity($user);
    }

    protected function persistPOSAdministratorEntity(Usuario $user)
    {
        $user->setRoles(array('ROLE_POS_ADMIN'));
        $user->setCreatedAt(new \DateTime('now'));
        $user->setUsername($user->getEmail());
        $this->userManager->updateUser($user, false);
        parent::persistEntity($user);
    }


    /*
    protected function updatePOSAdministratorEntity($user)
    {
        $canEmail = $user->getEmailCanonical();
        if ($user->getPlainPassword() != null && strlen($user->getPlainPassword()) > 0) {
            $this->firebase->getAuth()->changeUserPassword($user->getUid(), $user->getPlainPassword());
        }
        $properties = [
            'displayName' => $user->getNombre() . " " . $user->getApellido(),
            'emailverified' => true
        ];
        $this->firebase->getAuth()->setCustomUserAttributes($user->getUid(), $properties);
        $this->userManager->updateUser($user, false);
        $user->setEmailCanonical($canEmail);
        parent::updateEntity($user);
    }
    */

    /*
    protected function removePOSAdministratorEntity($user)
    {
        $this->firebase->getAuth()->deleteUser($user->getUid());
        $this->userManager->deleteUser($user);
        parent::removeEntity($user);
    }
    */

}
