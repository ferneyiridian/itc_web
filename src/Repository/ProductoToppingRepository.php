<?php

namespace App\Repository;

use App\Entity\ProductoTopping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductoTopping|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoTopping|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoTopping[]    findAll()
 * @method ProductoTopping[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoToppingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductoTopping::class);
    }

    // /**
    //  * @return ProductoTopping[] Returns an array of ProductoTopping objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductoTopping
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
