<?php

namespace App\Repository;

use App\Entity\ItemPromocion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemPromocion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPromocion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemPromocion[]    findAll()
 * @method ItemPromocion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemPromocionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemPromocion::class);
    }

    // /**
    //  * @return ItemPromocion[] Returns an array of ItemPromocion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ItemPromocion
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
