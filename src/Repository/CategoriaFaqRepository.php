<?php

namespace App\Repository;

use App\Entity\CategoriaFaq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoriaFaq|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriaFaq|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriaFaq[]    findAll()
 * @method CategoriaFaq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaFaqRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoriaFaq::class);
    }

    // /**
    //  * @return CategoriaFaq[] Returns an array of CategoriaFaq objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoriaFaq
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
