<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 22/05/2019
 * Time: 5:11 PM
 */

namespace App\Service;


use App\Entity\Categoria;
use App\Entity\CategoriaBlog;
use App\Entity\Compra;
use App\Entity\CompraItem;
use App\Entity\Direccion;
use App\Entity\EstadoPedido;
use App\Entity\Galeria;
use App\Entity\Image;
use App\Entity\Inventario;
use App\Entity\Journey;
use App\Entity\PagoSuscripcion;
use App\Entity\Post;
use App\Entity\Producto;
use App\Entity\Seo;
use App\Entity\Subcategoria;
use App\Entity\Suscripcion;
use App\Entity\Tarjeta;
use App\Entity\Tienda;
use App\Entity\Usuario;
use App\Entity\Video;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Google\Cloud\Firestore\FirestoreClient;
use function GuzzleHttp\Psr7\uri_for;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


define('MULTIPART_BOUNDARY', '----'.md5(time()));
define('EOL',"\r\n");

class QI
{
    protected $textosBig	= null;
    protected $textos		= null;
    protected $imagenes     = null;
    protected $settings		= null;
    private $session;


    public function __construct(EntityManagerInterface $em, RequestStack $request_stack, ContainerInterface $container, SessionInterface $session)
    {
        $this->em = $em;
        $this->request_stack = $request_stack;
        $this->container = $container;
        $this->session=$session;




        //$this->locale = $request_stack->getCurrentRequest()->getLocale();
    }

    public function crearPedido($metodo,$canal,$comentarios,$user){

        /** @var  $direccion Direccion */
        $direccion = $this->getDireccion();
        $direccion = $this->em->getRepository(Direccion::class)->find($direccion->getId());
        $estado = $this->em->getRepository(EstadoPedido::class)->findOneBy(array('ref'=>'INICIADA_EN_WEB'));

        /** @var  $compra Compra */
        $compra = new Compra();
        $compra->setUsuario($user);
        $compra->setDireccion($direccion);
        $compra->setEstado($estado);
        $compra->setTotal(0);
        $compra->setSubtotal(0);
        $compra->setComentarios($comentarios);
        $compra->setCanal($canal);
        $compra->setMetodo($metodo);
        $compra->setCostoDomicilio(0);

        $em = $this->em;
        $em->persist($compra);
        $em->flush();


        $subtotal=0;
        $impuestos=0;
        $impuestoTotal=0;

        $ciudad = $compra->getDireccion()->getCiudad();
        $nombreCiudad = $ciudad;

        $total = 0;
        $descripcion_text = '';
        $descripcion = '<table align="center" border="0" cellpadding="5px" cellspacing="0" class="mcnTextContentContainer" style="color: #999;font-family: Arial, Helvetica, sans-serif;max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">';
        $descripcion .= '<thead>
                            <tr>
                                <th></th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>';

        $iva = $this->getSetting('iva');
        $carrito = $this->getCarrito();
        $tienda = $this->getTienda();
        if($tienda)
            $tienda = $this->em->getRepository(Tienda::class)->find($tienda->getId());
        else
            $tienda = $this->em->getRepository(Tienda::class)->findOneBy(array());

        $costo_envio = $tienda->getCostoDomicilio() ;
        $costo_envio=floatval($costo_envio);

        foreach ($carrito as $item) {
            $cantidad = $item['cantidad'];

            if ($cantidad > 0) {

                $product_id=$item["producto"]->getPlu();
                $prod = $this->em->getRepository(Producto::class)->findOneBy(array('plu' => $product_id));
                if($this->getSetting('pedido_multitienda') != '1') {

                    $inv = $this->em->getRepository(Inventario::class)->findOneBy(array('producto' => $prod, 'tienda' => $tienda));
                }else{
                    $tienda =null;
                    $costo_envio = $this->getSetting('costo_domicilio');
                    $costo_envio=floatval($costo_envio);
                    $inv = $this->em->getRepository(Inventario::class)->findOneBy(array('producto' => $prod));
                }

                if ($inv) {
                    /** @var  $producto Producto */
                    $producto = $prod;
                    $subtotal = $cantidad * $inv->getPrecioBase($iva);
                    $impuestos = $cantidad * $inv->getImpuesto($iva);
                    $impuestoTotal = $impuestoTotal + $impuestos;
                    $total += $subtotal + $impuestos;
                    $compraitem = new CompraItem();
                    $compraitem->setCantidad($cantidad);
                    $compraitem->setProducto($producto);
                    $compraitem->setCompra($compra);
                    $compraitem->setPrecio($subtotal + $impuestos);
                    $em->persist($compraitem);
                    $em->flush();

                    $descripcion .= '<tr style="border-bottom: 1px solid #eae9e9;">';
                    $descripcion .= '<td>
                                        <img style="width: 70px;display:inline-block;vertical-align:middle" alt="" src="' . $producto->getImagen() . '" class="CToWUd">
                                    </td>';
                    $descripcion .= '<td style="text-align:center;"><p style="display:inline-block;vertical-align:middle">
                                        <span style="width:100%;display:inline-block;text-align: center;">' . $producto . '</span>
                                    </p></td>';
                    $descripcion .= '<td style="text-align:center;">' . $cantidad . '</td>';
                    $descripcion .= '<td style="text-align:center;"> $' . number_format($inv->getPrecio()) . '</td>';
                    $descripcion .= '<td style="text-align:center;"> $' . number_format($subtotal) . '</td>';
                    $descripcion .= '</tr>';

                    $descripcion_text .= '|' . $producto . ' - ';
                    $descripcion_text .= '$' . number_format(round($inv->getPrecio() , 2)) . ' - ';
                    $descripcion_text .= 'X' . $cantidad . ' - ';
                    $descripcion_text .= '$' . number_format(round($cantidad * $inv->getPrecio() , 2)) . ' | ';
                }
            }
        }
        $descripcion .= '<tr>
                        <td colspan="3"></td>
                        <td style="text-align: right;"><strong>Subtotal</strong></td>
                        <td style="text-align:center;">$'.number_format($total).'</td>
                    </tr>';
        $descripcion .= '<tr>
                        <td colspan="3"></td>
                        <td style="text-align: right;"><strong>Envio</strong></td>
                        <td style="text-align:center;">$'.number_format( $costo_envio).'</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td style="text-align: right;"><strong>Total</strong></td>
                        <td style="text-align:center;">$'.number_format($total + $costo_envio).'</td>
                    </tr>';

        $descripcion .= '</table>';

        $descripcion .= '<br>
                    <table align="center" border="0" cellpadding="5px" cellspacing="0" class="mcnTextContentContainer" style="text-align: center;border: 1px solid #eae9e9;color: #999;font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                        <tbody>
                            <tr style="border-bottom: 1px solid #eae9e9;">
                                <td style="    background-color: #A9D4EF; color:#fff;">
                                    <strong>Ciudad</strong>
                                </td>
                                <td>
                                    '.$direccion->getCiudad().'
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid #eae9e9;">
                                <td style="    background-color: #A9D4EF; color:#fff;">
                                    <strong>Dirección</strong>
                                </td>
                                <td>
                                    '.$direccion->getDireccion().'
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>';

        //$subtotal= $total;

        $compra->setSubtotal(round($total,2));
        $total = $total + $costo_envio;
        $compra->setTotal(round($total,2));
        $compra->setCostoDomicilio(round($costo_envio,2));
        $compra->setBody($descripcion);

        $compra->setTienda($tienda);
        $compra->setImpuestos(round($impuestoTotal,2));

        $em->persist($compra);
        $em->flush();
        $descripcion_text .= 'Costo envio, $' . number_format(round($costo_envio,2) ) . ' |';
        $descripcion_text .= 'TOTAL: $'.number_format(round($total,2) );

        return array(
            'compra'=>$compra,
            'descripcion'=>$descripcion_text
        );
    }


    public function crearTarjetaUsuario($user,$tarjeta){
        $u = $this->em->getRepository(Usuario::class)->find($user->getId());
        if( $u->getIdPayu() ){
            $cliente_id = $u->getIdPayu();
        }else{
            //crear cliente payu
            $cliente = $this->crearClientePayu(array('fullName'=>$user->getNombre().' '.$user->getApellido(),'email'=>$user->getEmail()));
            $cliente_id = $cliente->id;
            //actualizar user
            $u->setIdPayu($cliente->id);
            $user->setIdPayu($cliente->id);
            $this->em->persist($u);
        }

        //crear tarjeta a ese cliente
        $card = $this->crearTarjetaPayu($tarjeta,$cliente_id);
        if(property_exists($card,'type')){
            return $card;
        }
        if(!$card->token){
            return $card;
        }
        //crear tarjeta en nuestro sistema
        $newT = new Tarjeta();
        $newT->setToken($card->token);
        $newT->setUsuario($u);
        $this->em->persist($newT);
        //crear suscripcion a ese cliente y a esa tarjeta
        $cuotas = 1;
        $planId = $this->getSetting('plan_id_payu');
        $suscripcion=array('clienteId'=>$cliente_id,'tokenTarjeta'=>$card->token,'installments'=>$cuotas,'planId'=>$planId);
        $suscripcion=$this->crearSuscripcionPayu($suscripcion);

        //crear suscripcion en nuestro sistema
        $newS = new Suscripcion();
        $newS->setUsuario($u);
        $newS->setEmail($user->getEmail());
        $newS->setPayerId($cliente_id);
        $newS->setPlanId($planId);
        $newS->setStatus('creada_payu');
        $newS->setIdPayu($suscripcion->id);
        $newS->setActiva(false);
        $this->em->persist($newS);
        $this->em->flush();


        //crear pagos de esa suscripcion
        $resp = $this->getPagoSuscripcionPayu($suscripcion->id);

        $activa= false;
        foreach ($resp->recurringBillList as $p) {
            $pe = $this->em->getRepository(PagoSuscripcion::class)->findBy(array('id_payu'=>$p->id));
            if(count($pe) == 0 && false) {
                $pago = new PagoSuscripcion();
                $pago->setIdPayu($p->id);
                $pago->setOrderId($p->subscriptionId);
                $pago->setMonto($p->amount);
                $pago->setMoneda($p->currency);
                $fecha = new \DateTime();
                $fecha->setTimestamp($p->dateCharge);
                $pago->setFecha($fecha);
                $pago->setState($p->state);
                $pago->setSuscripcion($newS);

                $this->em->persist($pago);
                $this->em->flush();
                $this->saveFire($pago);
                if($p->state == 'completed'  || $p->state == 'PAID' ){
                    $activa = $activa || true;
                }
            }
        }
        if($activa){
            $newS->setActiva($activa);
            $this->em->persist($newS);
            $this->em->flush();
        }
        $this->saveFire($newS);
        //retornar
        return $newS->getId();

    }



    /*
     * params mixed $cliente (fullname,email)
     * */
    public function crearClientePayu($cliente){
        $postdata = json_encode($cliente);
        $url = $this->getSetting('urlpayu').'customers/';
        return $this->sendDataPayu($postdata,$url);

    }
    /*
     * params mixed $tarjeta "name": "Sample User Name",
       "document": "1020304050",
       "number": "4242424242424242",
       "expMonth": "01",
       "expYear": "2018",
       "type": "VISA",
       "address": {
          "line1": "Address Name",
          "line2": "17 25",
          "line3": "Of 301",
          "postalCode": "00000",
          "city": "City Name",
          "state": "State Name",
          "country": "CO",
          "phone": "300300300"
       }
     * */
    public function crearTarjetaPayu($tarjeta,$clienteId){
        $postdata = json_encode($tarjeta);
        $url = $this->getSetting('urlpayu').'customers/'.$clienteId.'/creditCards';
        return $this->sendDataPayu($postdata,$url);

    }

    /*
     * params mixed $suscripcion ($clienteId,$tokenTarjeta,$installments,$plan)
     * */
    public function crearSuscripcionPayu($suscripcion){

        $postdata = array(
            "quantity"=> "1",
            "installments"=> $suscripcion["installments"],
            "trialDays"=> "0",
            "immediatePayment"=> true,
            "customer"=> array(
                "id"=> $suscripcion["clienteId"],
                "creditCards"=> array(
                   array(
                     "token"=> $suscripcion["tokenTarjeta"]
                   )
                 )
            ),
            'notifyUrl'=>$this->getSetting('urlRespuestaPayu'),
            "plan"=> array(
                    "planCode"=> $suscripcion["planId"]
        ));
        $postdata = json_encode($postdata);
        $url = $this->getSetting('urlpayu').'subscriptions/';
        return $this->sendDataPayu($postdata,$url);

    }

    public function makeRefundPayu(PagoSuscripcion $pago,Suscripcion $suscripcion){

        $pagos = $this->getPagoSuscripcionPayu($suscripcion->getIdPayu());
        $orderid = '';
        $transactionid='';
        foreach ($pagos->recurringBillList as $p){

            if( strpos( $pago->getOrderId() ,$p->id)!== FALSE){
                $orderid = $p->orderId;
                $transactionid = $pago->getIdPayu();
            }
        }

        if($orderid != ''){
            $postdata = array(
                "language"=> "es",
                "command"=> "SUBMIT_TRANSACTION",
                "merchant"=> array(
                    "apiKey"=> $this->getSetting('payu_apiKey'),
                    "apiLogin"=> $this->getSetting('payu_apiLogin')
                ),
                "transaction"=> array(
                    "order"=> array(
                        "id"=> $orderid
                    ),
                    "type"=> "REFUND",
                    "reason"=> $this->getSetting('razon_cancelacion_duplicado_payu'),
                    "parentTransactionId"=> $transactionid
                )
            ,
                "test"=> false);
            $postdata = json_encode($postdata);
            $url = $this->getSetting('urlpayu_refund');
            return $this->sendDataPayu($postdata,$url);
        }else{
            return false;
        }



    }

    /*
    * params $suscripcionId
    * */
    public function getPagoSuscripcionPayu($suscripcionId){

        $url = $this->getSetting('urlpayu').'recurringBill?subscriptionId='.$suscripcionId;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($this->getSetting('payu_apiLogin').":".$this->getSetting('payu_apiKey')) ."\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;


        return $this->sendDataPayu($postdata,$url);

    }

    public function getPagosUsuarioPayu($clienteId){

        $url = $this->getSetting('urlpayu').'recurringBill?customerId='.$clienteId;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($this->getSetting('payu_apiLogin').":".$this->getSetting('payu_apiKey')) ."\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;


        return $this->sendDataPayu($postdata,$url);

    }

    private function sendDataPayu($postdata,$url){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic ".base64_encode($this->getSetting('payu_apiLogin').":".$this->getSetting('payu_apiKey')) ."\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata );
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;
    }

    private function getResultsSettings($entidad){
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from($entidad, 's', 's.name')
            ->where('s.name is not null')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getArrayResult();
        return $qb;
    }

    private function getResults($entidad){
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from($entidad, 's', 's.llave')
            ->where('s.llave is not null')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getArrayResult();
        return $qb;
    }

    /**
     * Retorna la variable con textos populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los textos intenacionalizados
     */
    private function getTextos() {
        if ($this->textos == null) {
            $this->textos = $this->getResults('App:Texto');
        }
        return $this->textos;
    }

    /**
     * Obtener uno de los texto fijos internacionalizados según el UserCulture actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El texto solicitado
     */
    public function getTexto($key) {
        $arrTextos = $this->getTextos();
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        if(isset($arrTextos[$key]) && $arrTextos[$key] != '')
            return $arrTextos[$key]['valor'];
        else
            return $key;
    }



    private function getTextosBig() {
        if ($this->textosBig == null) {
            $this->textosBig = $this->getResults('App:TextoBig');
        }
        return $this->textosBig;
    }

    public function getTextoBig($key) {
        $arrTextos = $this->getTextosBig();
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        if(isset($arrTextos[$key]) && $arrTextos[$key] != '')
            return $arrTextos[$key]['valor'];
        else
            return '';
    }/**
 * Retorna la variable con settings populada la primera vez que se llama esta función, los textos se traen de la base de datos
 * @return multitype: Arreglo con los settings
 */
    private function getSettings() {
        if ($this->settings == null) {
            $this->settings = $this->getResultsSettings('App:Setting');
        }
        return $this->settings;
    }


    /**
     * Obtener uno de los texto fijos internacionalizados según el local actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El setting solicitado
     */
    public function getSetting($key) {
        $settings = $this->getSettings();
        if(isset($settings[$key]) && $settings[$key]['value'] != '')
            return $settings[$key]['value'];
        else
            return '';
    }

        public function getGaleria($key){
        return $this->em->createQueryBuilder()
            ->from('App:Image','i')
            ->select('i')
            ->leftJoin('i.galeria','g')
            ->where('g.llave  = :llave')
            ->andWhere('i.visible = 1')
            ->setParameter('llave', $key)
            ->orderBy('i.orden', 'asc')
            ->getQuery()
            ->getResult();

    }

     public function getSubcategorias($id=null){
        $filtros = [];
        if($id) {
            $filtros["id"] = $id;
        }
        return $this->em->getRepository(Subcategoria::class)->findBy($filtros,array("orden"=>"desc"));
    }

    public function getCategoriaBlog($id=null){
        $filtros = [];
        if($id) {
            $filtros["id"] = $id;
        }
        return $this->em->getRepository(CategoriaBlog::class)->findBy($filtros,array("orden"=>"desc"));
    }

    public function getProductos($destacados,$subcategoria=null){
        $filtros = [];
        $filtros["visible"]=true;
        $filtros["padre"]=null;
        if($destacados) {
            $filtros["destacado"] = $destacados;
        }
        if($subcategoria) {
            $filtros["subcategoria"] = $subcategoria;
        }
        return $this->em->getRepository(Producto::class)->findBy($filtros,array("orden"=>"desc"));

    }

    public function getVideosCategoria($categoria){
        $filtros = [];
        $filtros["visible"]=true;
        $filtros["categoria"]=$categoria;


        return $this->em->getRepository(Video::class)->findBy($filtros,array("orden"=>"desc"));

    }





    public function getJourneys($destacados =false){
        if($destacados){
            return $this->em->getRepository(Journey::class)->findBy(array("destacado"=>$destacados),array("orden"=>"desc"));
        }else{
            return $this->em->getRepository(Journey::class)->findBy(array("visible"=>true),array("orden"=>"desc"));
        }

    }


    public function getCategorias(){
        return $this->em->getRepository(Categoria::class)->findBy(array("visible"=>true),array("orden"=>"desc"));
    }

    public function getPost($categoria = null){
        $filtros = [];
        if($categoria) {
            $filtros["categoria"] = $categoria;
        }
        return $this->em->getRepository(Post::class)->findBy($filtros,array("fecha"=>"desc"));
    }

    public function getSeo( $url, $homepage)
    {
        $url_abs = $url;
        $url = str_replace($homepage,'',$url);
        $path = $this->container->getParameter('app.path.product_images');
        $qb = $this->em->createQueryBuilder();
        $qb2 = $this->em->createQueryBuilder();
        //exit(dump($url));
        if($url == "")
            $url = "/";
        $seo = $qb
            ->select('p.id','p.titulo as titulo',"concat('".$path."/',p.image) as imagen",'p.descripcion as descripcion')
            ->from(Seo::class, 'p')
            ->where('p.url = :url')
            ->setParameter('url',$url)
            ->orderBy('p.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if($seo == null){
            $seo = $qb2
                ->select('s.id','s.titulo as titulo',"concat('".$path."/',s.image) as imagen",'s.descripcion as descripcion')
                ->from(Seo::class, 's')
                ->orderBy('s.id', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }
        return $seo;
    }

    private function getImagenes() {
        if ($this->imagenes == null) {
            $this->imagenes = $this->getResults('App:Image');
        }
        return $this->imagenes;
    }


    public function getImagen($key) {
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key];
        }
        else
            return [];
    }



    /**
     * @param $asunto
     * @param $to
     * @param $html
     * @return bool|string
     */
    public function sendMailIB($asunto,$to,$html){
        $postdata =array(
            'from' => "noreply@mailing.iridian.co",
            'to' => $to,
            'replyTo' => "noreply@mailing.iridian.co",
            'subject' => $asunto,
            'html' => $html,
            'intermediateReport'=> 'true'
        );
        return $this->send_mail_infoBip($postdata);
    }

    function send_mail_infoBip($postdata)
    {
        $url = 'https://api.infobip.com/email/1/send';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type:  multipart/form-data ; boundary=' . MULTIPART_BOUNDARY
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_USERPWD,'FERNANDOiridian123' . ":" . 'zuca2020');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$this->getBody($postdata) );
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        $information = curl_getinfo($ch);
        //die(dump($information));
        curl_close($ch);
        return $data;
    }

    public function sendMailPHP($asunto,$to,$html){

        try {
            $mail = new PHPMailer(true);
            $senderUser = $this->getSetting('sender_mail_user');
            $senderPass = $this->getSetting('sender_mail_pass');
            $senderHost = $this->getSetting('sender_mail_host');

            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $senderHost;                   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $senderUser;                 // SMTP username
            $mail->Password = $senderPass;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($senderUser, $this->getTexto('sender_mail_name'));
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('contact@example.com');

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $asunto;
            $mail->Body    = $html;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            return 'Message has been sent';
        } catch (Exception $e) {
            return 'Message could not be sent.';
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }

    function getBodyOld($fields) {
        $content = '';
        foreach ($fields as $FORM_FIELD => $value) {
            $content .= '--' . MULTIPART_BOUNDARY . EOL;
            $content .= 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . EOL;
            $content .= EOL . $value . EOL;
        }
        return $content . '--' . MULTIPART_BOUNDARY . '--'; // Email body should end with "--"
    }

    function getBody($fields) {
        $content = '';
        foreach ($fields as $FORM_FIELD => $value) {
            if ($FORM_FIELD == 'attachment') {
                $content .= '--' . MULTIPART_BOUNDARY . EOL;
                $content .= 'Content-Disposition: form-data; name="attachment"; filename="' . basename($value) .'"' . EOL;
                $content .= 'Content-Type: application/x-object' . EOL;
                $content .= EOL . file_get_contents($value) . EOL;
            } else {
                $content .= '--' . MULTIPART_BOUNDARY . EOL;
                $content .= 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . EOL;
                $content .= EOL . $value . EOL;
            }
        }
        return $content . '--' . MULTIPART_BOUNDARY . '--'; // Email body should end with "--"
    }

    /*
     * Method to get the headers for a basic authentication with username and passowrd
    */
    function getHeader($username, $password){
        // basic Authentication
        $auth = base64_encode("$username:$password");

        // Define the header
        return array("Authorization:Basic $auth", 'Content-Type: multipart/form-data ; boundary=' . MULTIPART_BOUNDARY );
    }

    /**
     * @param $subject
     * @param $from
     * @param $to
     * @param $custom
     * @param $template
     */
    public function sendMail($asunto,$to,$html){
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $senderUser = $this->getSetting('sender_mail_user');
            $senderPass = $this->getSetting('sender_mail_pass');
            $senderHost = $this->getSetting('sender_mail_host');
            $senderSubject = $this->getSetting('sender_mail_name');
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $senderHost;                   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $senderUser;                 // SMTP username
            $mail->Password = $senderPass;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($senderUser,$senderSubject);
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('contact@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $asunto;
            $mail->Body    = $html;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();

            return 'Message has been sent';
        } catch (\Exception $e) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }

    public function clearCar($wish){
        $ubicacion = 'carrito';

        if($wish){
            $ubicacion .= '_wish';
        }
        $carrito= [];
        $this->session->set($ubicacion,$carrito);
    }

    public function deleteItemFromCar($variante,$wish){
        $ubicacion = 'carrito';
        $variantep=$variante;

        if($wish){
            $ubicacion .= '_wish';
        }
        $carrito= $this->session->get($ubicacion,[]);
        unset($carrito[$variantep]);
        $this->session->set($ubicacion,$carrito);
    }

    public function addItemToCar($variante,$cant,$wish){
        $ubicacion = 'carrito';
        $variantep=$variante;

        if($wish){
            $ubicacion .= '_wish';
        }
        $carrito= $this->session->get($ubicacion,[]);
        $carrito[$variantep]=$cant;
        $this->session->set($ubicacion,$carrito);
    }

    public function addCantToCar($variante,$cant,$wish){
        //$this->session->set('journeys',[]);
        $variantep=$variante;
        $ubicacion = 'carrito';

        if($wish){
            $ubicacion .= '_wish';
        }
        $carrito= $this->session->get($ubicacion,[]);
        if($this->inCarrito($variante,$wish)){
            $carrito[$variantep]=$carrito[$variantep]+$cant;
            $this->session->set($ubicacion,$carrito);
        }else{
            $this->addItemToCar($variante,$cant,$wish);
        }

        return true;
    }

    public function inCarrito($variante,$wish){
        $ubicacion = 'carrito';
        $variantep=$variante;

        if($wish){
            $ubicacion .= '_wish';
        }
        $carrito= $this->session->get($ubicacion,[]);
        return isset($carrito[$variantep]);
    }

    public function setDireccion($dir){
        $this->session->set('direccion',$dir);
    }

    public function getDireccion(){
        return $this->session->get('direccion');
    }

    public function setTienda($dir){
        $this->session->set('tienda',$dir);
    }

    /**
     * @return Tienda
     */
    public function getTienda(){
        return $this->session->get('tienda');
    }

    function splitjourney($id)
    {
        return explode("|",$id)[0];
    }

    public function carritoLength(){
        $carrito = $this->session->get('carrito',[]);
        return count($carrito);
    }


    public function getCarrito(){
        $carrito= $this->session->get('carrito',[]);
        $prods= [];
        $tienda = $this->getTienda();

        $padres=$this->em->getRepository(Producto::class)->findBy(array("plu"=>array_keys($carrito)));
        foreach ($padres as $h){
            foreach ($carrito as $c=>$v){
                if($v>0) {
                    if ($h->getPlu() == $c) {
                        if($this->getSetting('pedido_multitienda') != 1)
                            $inv = $this->em->getRepository(Inventario::class)->findOneBy(array("producto" => $h, "tienda" => $tienda));
                        else
                            $inv = $this->em->getRepository(Inventario::class)->findOneBy(array("producto" => $h));
                        if ($inv) {
                            $prods[$c]["producto"] = $h;
                            $prods[$c]["cantidad"] = $v;

                            $prods[$c]["precio"] = $inv->getPrecio();
                        } else {
                            $this->deleteItemFromCar($h->getPlu(), "");
                        }
                    }
                }else{
                    $this->deleteItemFromCar($c, "");
                }
            }
        }

        return $prods;
    }

    public function getCarritoj(){
       $carrito= $this->session->get('carrito',[]);
        $prods= [];

        $journeys=$this->em->getRepository(Journey::class)->findBy(array("id"=>array_keys($carrito)));


        foreach ($journeys as $j){
            foreach ($carrito as $c=>$v){
                if($j->getId() == $c){
                    $prods[$c]["journey"]=$j;
                    $prods[$c]["cantidad"]=$v;
                    $prods[$c]["libro"]=$j->getLIbro();
                    $prods[$c]["cantlibro"]=1;
                    $prods[$c]["gafas"]=$j->getGafas();
                    $prods[$c]["cantgafas"]=1;
                    $prods[$c]["ropa"]=$j->getRopa();
                    $prods[$c]["cantropa"]=1;

                }
            }
        }

         $productos=$this->em->getRepository(Producto::class)->findBy(array("id"=>array_keys($carrito)));
        foreach ($productos as $p) {
            foreach ($carrito as $c => $v) {
                if ($p->getId() == $c) {
                    $prods[$c]["producto"] = $p;
                    $prods[$c]["cantpro"] = $v;
                }
            }
        }
        /*$hijos=$this->em->getRepository(Producto::class)->findBy(array("id"=>array_keys($carrito),'visible'=>true));
        foreach ($hijos as $h){
            foreach ($carrito as $c=>$v){
                if($h->getId() == $c && $h->getPadre() != null){

                    $papa=$h->getPadre();
                    $papa= clone($papa);
                    $papa->setId($h->getId());
                    $imagen= $papa->getImage();
                    if($h->getNombre()!= null && $h->getNombre() != ""){
                        $papa->setNombre($h->getNombre());
                    }
                    if($h->getVariantes()!= null ){
                        $papa->setVariantes($h->getVariantes());
                    }
                    $prods[$c]["producto"]=$papa;
                    $prods[$c]["cantidad"]=$v;



                }
            }
        }

        $padres=$this->em->getRepository(Producto::class)->findBy(array("id"=>array_keys($carrito)));
        foreach ($padres as $h){
            foreach ($carrito as $c=>$v){
                if($h->getId() == $c && $h->getPadre() == null){
                    $imagen= $h->getImage();
                    $prods[$c]["producto"]=$h;
                    $prods[$c]["cantidad"]=$v;
                }
            }
        }*/


        return $prods;
    }

    public function getProductosPorCompra($compra){
        return $this->em->createQueryBuilder()
            ->from('App:CompraItem','ci')
            ->select('ci.cantidad','p.nombre','p.id')
            ->leftJoin('ci.producto','p')
            ->where('ci.compra = :compra')
            ->setParameter('compra', $compra)
            ->getQuery()
            ->getArrayResult();

    }

    /**
     * @return Categoria[]
     */
    public function getCategoriasVeci(){
        $categorias = $this->em->getRepository('App:Categoria')->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.productos','p')
            ->leftJoin('p.inventarios','i')
            ->where('i.cantidad > 0')
            ->where('i.tienda = :tienda')
            ->setParameter('tienda',$this->getTienda()->getId())
            ->groupBy('c')
            ->getQuery()
            ->getArrayResult();
        return $categorias;
    }

    /**
     * @return Categoria[]
     */
    public function getCategoriasConProductos(){
        $categorias = $this->em->getRepository('App:Categoria')->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.productos','p')
            ->leftJoin('p.inventarios','i')
            ->where('i.cantidad > 0')
            ->orderBy('c.orden','asc')
            ->groupBy('c')
            ->getQuery()
            ->getArrayResult();
        return $categorias;
    }



    /**
     * @param $entity
     */
    public function saveFire($entity, $extra = []){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $metadata = $em->getClassMetadata(get_class($entity));
        $campos = $metadata->fieldNames;
        $accessor = PropertyAccess::createPropertyAccessor();
        $arr = array();
        foreach ($campos as $campo){
            $valor = $accessor->getValue($entity,$campo);
            if($valor instanceof \DateTime){
                $arr[$campo] = $valor->format("Y-m-d H:i:s");
            }else{
                $arr[$campo] = $valor;
            }
        }
        $campos = $metadata->associationMappings;
        foreach ($campos as $campo){

            $campo = $campo['fieldName'];
            $valor = $accessor->getValue($entity,$campo);
            $arr_in = array();
            if($valor instanceof PersistentCollection){
                foreach ($valor as $inner){
                    array_push($arr_in,$inner->getId());
                }
                $arr[$campo] = $arr_in;
            }else{
                if(is_string($valor) || $valor == null){
                }else{
                    if(method_exists($valor,'getId'))
                        $arr[$campo] = $valor->getId();
                }
            }
        }
        if(array_key_exists('categorias',$entity)){
            $arr['categorias'] = $entity['categorias'];
        }
        foreach ($extra as $key=>$value){
            $arr[$key] = $value;
        }
        $json = json_encode($arr);

        $tabla = explode(':',get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        //$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../miveci-firebase-adminsdk-pteyb-91be53dc7d.json');

        if($this->getSetting('habilitar_firestore') == "1"){
            $db = new FirestoreClient(array('projectId'=>$this->getSetting('projectId')));
            $docRef = $db->collection($tabla)->document($entity->getId());
            if($entity->getId()) {
                $res = $docRef->set($arr);
            }
        }


        //$ref = $database->getReference("/$tabla/".$entity->getId())->set($arr);
    }

    /**
     * @param $entity
     */
    public function removeFire($tabla, $id){
        if($this->getSetting('habilitar_firestore') == "1") {
            $db = new FirestoreClient(array('projectId' => $this->getSetting('projectId')));
            $docRef = $db->collection($tabla)->document($id)->delete();
        }
    }

    public function canastaBasica(Tienda $tienda){
    }


    /**
     * @param $entity
     */
    public function saveMultiFire($tabla, $array){
        if($this->getSetting('habilitar_firestore') == "1") {
            $db = new FirestoreClient(array('projectId' => $this->getSetting('projectId')));
            //$db->batch();
        }
        foreach ($array as $prod){
            /*
            $docRef = $db->collection($tabla)->document($prod['plu']);
            $res = $docRef->set($prod);
            */
        }
        //$database->getReference("/$tabla/".$entity->getId())->set($arr);
    }
}
