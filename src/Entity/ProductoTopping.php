<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="producto_topping_uk_idx", columns={"producto_id", "nombre"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductoToppingRepository")
 */
class ProductoTopping
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $opciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="toppings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $producto;

    /**
     * @ORM\Column(type="integer",options={"default" : 0})
     */
    private $maxToppings = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getOpciones()
    {
        return $this->opciones;
    }

    public function setOpciones($opciones): self
    {
        $this->opciones = $opciones;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getMaxToppings(): ?int
    {
        return $this->maxToppings;
    }

    public function setMaxToppings(int $maxToppings): self
    {
        $this->maxToppings = $maxToppings;

        return $this;
    }

    public function __toString()
    {
        return implode(',', $this->opciones);
    }


}
