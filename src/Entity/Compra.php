<?php

namespace App\Entity;

use App\Repository\CompraItemRepository;
use App\Repository\CompraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompraRepository")
 */
class Compra
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_entrega;

    /**
     * @ORM\Column(type="float")
     */
    private $costo_domicilio;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EstadoPedido")
     * @ORM\JoinColumn(nullable=false)
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MetodoPago")
     * @ORM\JoinColumn(nullable=false)
     */
    private $metodo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PagaCon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direccion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaProcesamiento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estadoProcesamiento;



    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaRealProcesamiento;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;



    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="text")
     */
    private $comentarios;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $canal;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CompraItem", mappedBy="compra", orphanRemoval=true)
     */
    private $compraitems;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $body;



    /**
     * @ORM\OneToMany(targetEntity=PagoPayu::class, mappedBy="compra")
     */
    private $pagos;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $impuestos;

    /**
     * @ORM\ManyToOne(targetEntity=Tienda::class)
     */
    private $tienda;

    public function __construct()
    {
        $this->compraitems = new ArrayCollection();
        $this->pagos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaEntrega(): ?\DateTimeInterface
    {
        return $this->fecha_entrega;
    }

    public function setFechaEntrega(?\DateTimeInterface $fecha_entrega): self
    {
        $this->fecha_entrega = $fecha_entrega;

        return $this;
    }

    public function getCostoDomicilio(): ?float
    {
        return $this->costo_domicilio;
    }

    public function setCostoDomicilio(float $costo_domicilio): self
    {
        $this->costo_domicilio = $costo_domicilio;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getEstado(): ?EstadoPedido
    {
        return $this->estado;
    }

    public function setEstado(?EstadoPedido $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getMetodo(): ?MetodoPago
    {
        return $this->metodo;
    }

    public function setMetodo(?MetodoPago $metodo): self
    {
        $this->metodo = $metodo;

        return $this;
    }

    public function getPagaCon(): ?float
    {
        return $this->PagaCon;
    }

    public function setPagaCon(?float $PagaCon): self
    {
        $this->PagaCon = $PagaCon;

        return $this;
    }

    public function getDireccion(): ?Direccion
    {
        return $this->direccion;
    }

    public function setDireccion(?Direccion $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getFechaProcesamiento(): ?\DateTimeInterface
    {
        return $this->fechaProcesamiento;
    }

    public function setFechaProcesamiento(?\DateTimeInterface $fechaProcesamiento): self
    {
        $this->fechaProcesamiento = $fechaProcesamiento;

        return $this;
    }

    public function getEstadoProcesamiento(): ?string
    {
        return $this->estadoProcesamiento;
    }

    public function setEstadoProcesamiento(?string $estadoProcesamiento): self
    {
        $this->estadoProcesamiento = $estadoProcesamiento;

        return $this;
    }




    public function getFechaRealProcesamiento(): ?\DateTimeInterface
    {
        return $this->fechaRealProcesamiento;
    }

    public function setFechaRealProcesamiento(?\DateTimeInterface $fechaRealProcesamiento): self
    {
        $this->fechaRealProcesamiento = $fechaRealProcesamiento;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function __toString()
    {
        return "#" . $this->id;
    }

    public function getComentarios(): ?string
    {
        return $this->comentarios;
    }

    public function setComentarios(string $comentarios): self
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    public function getCanal(): ?string
    {
        return $this->canal;
    }

    public function setCanal(?string $canal): self
    {
        $this->canal = $canal;

        return $this;
    }





    /**
     * @return Collection|CompraItem[]
     */
    public function getCompraitems(): Collection
    {
        return $this->compraitems;
    }

    public function addCompraitem(CompraItem $compraItem): self
    {
        if (!$this->compraitems->contains($compraItem)) {
            $this->compraitems[] = $compraItem;
            $compraItem->setCompra($this);
        }

        return $this;
    }

    public function removeCompraitem(CompraItem $compraItem): self
    {
        if ($this->compraitems->contains($compraItem)) {
            $this->compraitems->removeElement($compraItem);
            // set the owning side to null (unless already changed)
            if ($compraItem->getCompra() === $this) {
                $compraItem->setCompra(null);
            }
        }

        return $this;
    }


    public function getUsuarioFull(): ?string
    {
        $usuario = $this->getUsuario();
        $email = $this->getUsuario()->getEmail();
        $celular = $this->getUsuario()->getCelular();
        $direccion = $this->getDireccion()->getDireccion();
        $lat = $this->getDireccion()->getLat();
        $lng = $this->getDireccion()->getLng();
        $ciudad = $this->getDireccion()->getCiudad()->getNombre();
        $tabla = "<table>";
        $tabla .= "<tr><td>Nombre:</td><td>$usuario</td></tr>";
        $tabla .= "<tr><td>Celular:</td><td>$celular</td></tr>";
        $tabla .= "<tr><td>Email:</td><td>$email</td></tr>";
        $tabla .= "<tr><td>Direccion:</td><td><a target='_blank' href='https://www.google.com/maps/?q=$lat,$lng'>$direccion</a></td></tr>";
        $tabla .= "<tr><td>Ciudad:</td><td>$ciudad</td></tr>";
        $tabla .= "</table>";
        return $tabla;
    }


    public function getEstadoFull(): ?string
    {
        date_default_timezone_set('America/Bogota');
        $estado = $this->getEstado()->getNombre();
        if($this->getMetodo()->getId() == 1)
            $estado = 'Pago Contraentrega';
        $procesamiento = "";
        if($this->getFechaProcesamiento())
            $procesamiento = $this->getFechaProcesamiento()->format("d-m-Y h:i a");
        $procesamientoReal = "";
        if($this->getFechaRealProcesamiento())
            $procesamientoReal = $this->getFechaRealProcesamiento()->format("d-m-Y h:i a");
        $tabla = "<table>";
        $tabla .= "<tr><td>Recibido:</td><td>$procesamientoReal</td></tr>";
        $tabla .= "<tr><td>Estado:</td><td>$estado</td></tr>";
        $tabla .= "</table>";
        return $tabla;
    }

    public function getProductos(): ?string{
        $items = $this->getCompraitems();
        $tabla = "<table>";
        $tabla .= "<thead>";
        $tabla .= "<tr><th>Producto</th><th>Cant&nbsp;&nbsp;&nbsp;</th><th>Vl. Uni</th><th>Total</th></tr>";
        $tabla .= "</thead>";
        foreach ($items as $item){
            $nombre = $item->getProducto()->getNombre();
            if($item->getToppings())
                $nombre .= $item->getToppings();
            $cantidad = $item->getCantidad();
            $precio = $item->getPrecio();
            $precio_text = number_format($precio);
            $total = $cantidad*$precio;
            $total_text = number_format($total);
            $promo = "";
            if($item->getPromocion())
                $promo = $item->getPromocion()->getNombre();
            $tabla .= "<tr><td>$nombre</td><td>$cantidad</td><td>$$precio_text</td><td>$$total_text</td></tr>";
        }
        $comentarios = $this->getComentarios();
        $tabla .= "<tr><td>Comentarios</td><td colspan='4'>$comentarios</td></tr>";
        $costo = number_format($this->getCostoDomicilio());
        $tabla .= "<tr><td>Costo domicilio</td><td colspan='4'>$costo</td></tr>";
        $tabla .= "</table>";


        return $tabla;
    }


    public function getPago(): ?string
    {
        $total = number_format($this->getTotal());
        $metodo = $this->getMetodo()->getNombre();
        $paga_con = number_format($this->getPagaCon());
        $tabla = "<table>";
        $tabla .= "<tr><td>Total:</td><td>$total</td></tr>";
        $tabla .= "<tr><td>Método:</td><td>$metodo</td></tr>";
        $tabla .= "<tr><td>Paga con:</td><td>$paga_con</td></tr>";
        $tabla .= "</table>";
        return $tabla;
    }

    public function getChannel(){
        $canal = $this->getCanal();
        return "<img src='/img/back/$canal.png'/>";{{}}
    }

    /**
     * @return Collection|Intento[]
     */
    public function getIntentoslog(): Collection
    {
        return $this->intentoslog;
    }

    public function addIntentoslog(Intento $intentoslog): self
    {
        if (!$this->intentoslog->contains($intentoslog)) {
            $this->intentoslog[] = $intentoslog;
            $intentoslog->setCompra($this);
        }

        return $this;
    }

    public function removeIntentoslog(Intento $intentoslog): self
    {
        if ($this->intentoslog->contains($intentoslog)) {
            $this->intentoslog->removeElement($intentoslog);
            // set the owning side to null (unless already changed)
            if ($intentoslog->getCompra() === $this) {
                $intentoslog->setCompra(null);
            }
        }

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }



    /**
     * @return Collection|PagoPayu[]
     */
    public function getPagos(): Collection
    {
        return $this->pagos;
    }

    public function addPago(PagoPayu $pago): self
    {
        if (!$this->pagos->contains($pago)) {
            $this->pagos[] = $pago;
            $pago->setCompra($this);
        }

        return $this;
    }

    public function removePago(PagoPayu $pago): self
    {
        if ($this->pagos->contains($pago)) {
            $this->pagos->removeElement($pago);
            // set the owning side to null (unless already changed)
            if ($pago->getCompra() === $this) {
                $pago->setCompra(null);
            }
        }

        return $this;
    }

    public function getImpuestos(): ?float
    {
        return $this->impuestos;
    }

    public function setImpuestos(?float $impuestos): self
    {
        $this->impuestos = $impuestos;

        return $this;
    }

    public function getTienda(): ?Tienda
    {
        return $this->tienda;
    }

    public function setTienda(?Tienda $tienda): self
    {
        $this->tienda = $tienda;

        return $this;
    }


}
