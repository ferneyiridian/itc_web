<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\POSAdminRepository")
 */
class POSAdmin extends Usuario
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda", inversedBy="posAdmins")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tienda;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTienda(): ?Tienda
    {
        return $this->tienda;
    }

    public function setTienda(?Tienda $tienda): self
    {
        $this->tienda = $tienda;

        return $this;
    }
}
