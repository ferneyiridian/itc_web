<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="producto_tienda_uk_idx", columns={"producto_id", "tienda_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\InventarioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Inventario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="inventarios")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id", name="producto_id")
     */
    private $producto;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id", name="tienda_id")
     */
    private $tienda;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precio;

    public function __toString()
    {
        return $this->getProducto()->getPlu(). ' - '. $this->getTienda()->getNombre();
    }

    /**
     * Tienda constructor.
     */
    public function __construct()
    {
        $this->updatedAt = new \DateTime("now");
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getTienda(): ?Tienda
    {
        return $this->tienda;
    }

    public function setTienda(?Tienda $tienda): self
    {
        $this->tienda = $tienda;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(?float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getPrecioBase($iva){
        return $this->precio/(1+($iva/100));
    }

    public function getImpuesto($iva){
        return $this->getPrecioBase($iva)*($iva/100);
    }
}
