<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CargaRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uk_idx", columns={"gmail_id", "nombre"})})
 * @UniqueEntity(
 *     fields={"gmailId", "nombre"}
 *   )
 */
class Carga
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estado;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gmailId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gmailAsunto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gmailFrom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gmailDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resultado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getGmailId(): ?string
    {
        return $this->gmailId;
    }

    public function setGmailId(string $gmailId): self
    {
        $this->gmailId = $gmailId;

        return $this;
    }

    public function getGmailAsunto(): ?string
    {
        return $this->gmailAsunto;
    }

    public function setGmailAsunto(string $gmailAsunto): self
    {
        $this->gmailAsunto = $gmailAsunto;

        return $this;
    }

    public function getGmailFrom(): ?string
    {
        return $this->gmailFrom;
    }

    public function setGmailFrom(?string $gmailFrom): self
    {
        $this->gmailFrom = $gmailFrom;

        return $this;
    }

    public function getGmailDate(): ?string
    {
        return $this->gmailDate;
    }

    public function setGmailDate(?string $gmailDate): self
    {
        $this->gmailDate = $gmailDate;

        return $this;
    }

    public function getResultado(): ?string
    {
        return $this->resultado;
    }

    public function setResultado(?string $resultado): self
    {
        $this->resultado = $resultado;

        return $this;
    }
}
