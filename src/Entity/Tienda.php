<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TiendaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tienda
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, name="nombre")
     */
    private $nombre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"default":0})
     */
    private $orden = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $horarios_text;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $domicilios;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lng;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kml_id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $store_id_mu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\POSAdmin", mappedBy="tienda")
     */
    private $posAdmins;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ciudad")
     */
    private $ciudad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $productosfull;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $radio_cobertura;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $costo_domicilio = 0;

    /**
     * Tienda constructor.
     */
    public function __construct()
    {
        $this->visible = false;
        $this->updatedAt = new \DateTime("now");
        $this->posAdmins = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getHorariosText(): ?string
    {
        return $this->horarios_text;
    }

    public function setHorariosText(?string $horarios_text): self
    {
        $this->horarios_text = $horarios_text;

        return $this;
    }

    public function getDomicilios(): ?bool
    {
        return $this->domicilios;
    }

    public function setDomicilios(bool $domicilios): self
    {
        $this->domicilios = $domicilios;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?string
    {
        return $this->lng;
    }

    public function setLng(string $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getKmlId(): ?string
    {
        return $this->kml_id;
    }

    public function setKmlId(?string $kml_id): self
    {
        $this->kml_id = $kml_id;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getStoreIdMu(): ?string
    {
        return $this->store_id_mu;
    }

    public function setStoreIdMu(?string $store_id_mu): self
    {
        $this->store_id_mu = $store_id_mu;

        return $this;
    }

    /**
     * @return Collection|POSAdmin[]
     */
    public function getPosAdmins(): Collection
    {
        return $this->posAdmins;
    }

    public function addPosAdmin(POSAdmin $posAdmin): self
    {
        if (!$this->posAdmins->contains($posAdmin)) {
            $this->posAdmins[] = $posAdmin;
            $posAdmin->setTienda($this);
        }

        return $this;
    }

    public function removePosAdmin(POSAdmin $posAdmin): self
    {
        if ($this->posAdmins->contains($posAdmin)) {
            $this->posAdmins->removeElement($posAdmin);
            // set the owning side to null (unless already changed)
            if ($posAdmin->getTienda() === $this) {
                $posAdmin->setTienda(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * @param mixed $orden
     */
    public function setOrden($orden): void
    {
        $this->orden = $orden;
    }



    public function __toString()
    {
        return $this->nombre . " (" . $this->id . ")";
    }

    public function getCiudad(): ?Ciudad
    {
        return $this->ciudad;
    }

    public function setCiudad(?Ciudad $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getProductosfull(): ?bool
    {
        return $this->productosfull;
    }

    public function setProductosfull(?bool $productosfull): self
    {
        $this->productosfull = $productosfull;

        return $this;
    }

    public function getRadioCobertura(): ?float
    {
        return $this->radio_cobertura;
    }

    public function setRadioCobertura(?float $radio_cobertura): self
    {
        $this->radio_cobertura = $radio_cobertura;

        return $this;
    }

    public function getCostoDomicilio(): ?float
    {
        return $this->costo_domicilio;
    }

    public function setCostoDomicilio(?float $costo_domicilio): self
    {
        $this->costo_domicilio = $costo_domicilio;

        return $this;
    }


}
