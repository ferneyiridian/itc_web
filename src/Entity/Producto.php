<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Producto
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=100, name="id")
     */
    private $plu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $costo;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categoria", inversedBy="productos")
     */
    private $categorias;


    /**
     * @var \Doctrine\Common\Collections\Collection|Supergrupo[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\SuperGrupo")
     * @ORM\JoinTable(
     *  name="producto_supergrupo",
     *  joinColumns={
     *      @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="ref_id", referencedColumnName="ref")
     *  }
     * )
     */
    private $supergrupos;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductoTopping", mappedBy="producto", orphanRemoval=true)
     */
    private $toppings;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Momento", mappedBy="productos")
     */
    private $momentos;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $tiene_toppings;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sugerido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pum;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inventario", mappedBy="producto", cascade={"persist"})
     */
    private $inventarios;

    public function __construct()
    {
        $this->categorias = new ArrayCollection();
        $this->supergrupos = new ArrayCollection();
        $this->updatedAt = new \DateTime("now");
        $this->toppings = new ArrayCollection();
        $this->momentos = new ArrayCollection();
        $this->inventarios = new ArrayCollection();
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getResumen(): ?string
    {
        return $this->resumen;
    }

    public function setResumen(?string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getCosto(): ?float
    {
        return $this->costo;
    }

    public function setCosto(?float $costo): self
    {
        $this->costo = $costo;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getDestacado(): ?bool
    {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self
    {
        $this->destacado = $destacado;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return Collection|Categoria[]
     */
    public function getCategorias(): Collection
    {
        return $this->categorias;
    }

    public function addCategoria(Categoria $categoria): self
    {
        if (!$this->categorias->contains($categoria)) {
            $this->categorias[] = $categoria;
        }

        return $this;
    }

    public function removeCategoria(Categoria $categoria): self
    {
        if ($this->categorias->contains($categoria)) {
            $this->categorias->removeElement($categoria);
        }

        return $this;
    }

    public function getId()
    {
        return $this->plu;
    }

    public function getPlu(): ?string
    {
        return $this->plu;
    }

    public function setPlu(string $plu): self
    {
        $this->plu = $plu;

        return $this;
    }

    /**
     * @return Collection|Supergrupo[]
     */
    public function getSupergrupos(): Collection
    {
        return $this->supergrupos;
    }

    public function addSupergrupo(Supergrupo $supergrupo): self
    {
        if (!$this->supergrupos->contains($supergrupo)) {
            $this->supergrupos[] = $supergrupo;
        }

        return $this;
    }

    public function removeSupergrupo(Supergrupo $supergrupo): self
    {
        if ($this->supergrupos->contains($supergrupo)) {
            $this->supergrupos->removeElement($supergrupo);
        }

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function __toString()
    {
        return $this->nombre.' - '.$this->plu;
    }

    /**
     * @return Collection|ProductoTopping[]
     */
    public function getToppings(): Collection
    {
        return $this->toppings;
    }

    public function addTopping(ProductoTopping $topping): self
    {
        if (!$this->toppings->contains($topping)) {
            $this->toppings[] = $topping;
            $topping->setProducto($this);
        }

        return $this;
    }

    public function removeTopping(ProductoTopping $topping): self
    {
        if ($this->toppings->contains($topping)) {
            $this->toppings->removeElement($topping);
            // set the owning side to null (unless already changed)
            if ($topping->getProducto() === $this) {
                $topping->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Momento[]
     */
    public function getMomentos(): Collection
    {
        return $this->momentos;
    }

    public function addMomento(Momento $momento): self
    {
        if (!$this->momentos->contains($momento)) {
            $this->momentos[] = $momento;
            $momento->addProducto($this);
        }

        return $this;
    }

    public function removeMomento(Momento $momento): self
    {
        if ($this->momentos->contains($momento)) {
            $this->momentos->removeElement($momento);
            $momento->removeProducto($this);
        }

        return $this;
    }

    public function getTieneToppings(): ?bool
    {
        return $this->tiene_toppings;
    }

    public function setTieneToppings(?bool $tiene_toppings): self
    {
        $this->tiene_toppings = $tiene_toppings;

        return $this;
    }

    public function getSugerido(): ?bool
    {
        return $this->sugerido;
    }

    public function setSugerido(?bool $sugerido): self
    {
        $this->sugerido = $sugerido;

        return $this;
    }

    public function getPum(): ?string
    {
        return $this->pum;
    }

    public function setPum(?string $pum): self
    {
        $this->pum = $pum;

        return $this;
    }

    public function getPrecioBase($iva){
        return $this->precio/(1+($iva/100));
    }

    public function getImpuesto($iva){
        return $this->getPrecioBase($iva)*($iva/100);
    }



    /**
     * @return Collection|Inventario[]
     */
    public function getInventarios(): Collection
    {
        return $this->inventarios;
    }

    public function addInventario(Inventario $inventario): self
    {
        if (!$this->inventarios->contains($inventario)) {
            $this->inventarios[] = $inventario;
            $inventario->setProducto($this);
        }

        return $this;
    }

    public function removeInventario(Inventario $inventario): self
    {
        if ($this->inventarios->contains($inventario)) {
            $this->inventarios->removeElement($inventario);
            // set the owning side to null (unless already changed)
            if ($inventario->getProducto() === $this) {
                $inventario->setProducto(null);
            }
        }

        return $this;
    }


}
