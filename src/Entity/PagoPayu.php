<?php

namespace App\Entity;

use App\Repository\PagoPayuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PagoPayuRepository::class)
 */
class PagoPayu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idPayu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referenceSale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $monto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referencePol;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity=Compra::class, inversedBy="pagos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $compra;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPayu(): ?string
    {
        return $this->idPayu;
    }

    public function setIdPayu(?string $idPayu): self
    {
        $this->idPayu = $idPayu;

        return $this;
    }

    public function getReferenceSale(): ?string
    {
        return $this->referenceSale;
    }

    public function setReferenceSale(?string $referenceSale): self
    {
        $this->referenceSale = $referenceSale;

        return $this;
    }

    public function getMonto(): ?string
    {
        return $this->monto;
    }

    public function setMonto(?string $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getCus(): ?string
    {
        return $this->cus;
    }

    public function setCus(?string $cus): self
    {
        $this->cus = $cus;

        return $this;
    }

    public function getReferencePol(): ?string
    {
        return $this->referencePol;
    }

    public function setReferencePol(?string $referencePol): self
    {
        $this->referencePol = $referencePol;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCompra(): ?Compra
    {
        return $this->compra;
    }

    public function setCompra(?Compra $compra): self
    {
        $this->compra = $compra;

        return $this;
    }
}
